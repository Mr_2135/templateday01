package com.aaa.demo;

import cn.dev33.satoken.SaManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author Mr.2135
 * @author demo启动类
 */
@SpringBootApplication
@EnableAsync
@Slf4j
/**
 * 扫描mybatis mapper 包路径(如果启动找不到beam，有可能是这边路径错误)
 */
//@MapperScan(basePackages = "com.aaa.demo.mapper")
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
        log.info("启动成功：sa-token配置如下：{}", SaManager.getConfig());
    }

}
