package com.aaa.demo.aop;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.ReflectUtil;
import com.aaa.demo.entity.TblUser;
import com.aaa.demo.mapper.TblUserMapper;
import com.aaa.demo.util.MyConstants;
import com.aaa.demo.util.NbUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @Author:
 * @Date:  2021-08-12 16:08
 * @Version 1.0
 * 补充添加修改信息
 */
@Aspect
@Component
@Slf4j
public class EntityAspect {

    @Autowired
    private TblUserMapper userMapper;

    @Pointcut("@annotation(com.aaa.demo.aop.SaveOrUpdateEntityAnn)")
    public void pointCutSaveOrUpdate(){}
    @Around("pointCutSaveOrUpdate()")
    /**
     * create by: Teacher王
     * description: 通过AOP环绕方法将controller中的保存和修改方法统一
     * 加上时间和人
     * create time: 2020/6/17 10:
     * @Param: joinPoint
     * @return java.lang.Object
     */
    public Object aroundMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        //获取所有的参数
        Object[] joinPointArgs = joinPoint.getArgs();
        //获取方法的签名
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        //获取方法
        Method method = signature.getMethod();
        String methodName= method.getName();
//        log.info("获取到参数：{}",methodName);
        //获取注解
        SaveOrUpdateEntityAnn annotation = method.getAnnotation(SaveOrUpdateEntityAnn.class);
        //获取注解的值
        Class<?> entityClass = annotation.entityClass();
        //遍历所有的参数，并修改参数的内容
        for (int i = 0; i <joinPointArgs.length ; i++) {
            Object fromObject=joinPointArgs[i];
            //通过jackson工具转换对象
            ObjectMapper objectMapper = new ObjectMapper();
            //将原始对象，通过jakson工具转换成具体的实体对象
            Object toObject = objectMapper.convertValue(fromObject,entityClass);
//            log.info("获取到的实体类：{}",toObject);
            //获取当前登录的用户
            QueryWrapper<TblUser> wrapper = new QueryWrapper<>();
            wrapper.eq("user_id",StpUtil.getLoginId());
            TblUser user = userMapper.selectOne(wrapper);
            //如果是保存方法，自动匹配创建人和创建时间
            if(methodName.contains(MyConstants.SAVE_OPERATION)){
                ReflectUtil.invoke(toObject,"setCreateTime", LocalDateTime.now());
                ReflectUtil.invoke(toObject,"setCreateBy",user.getUserName());
//                Method setCreateTime = entityClass.getMethod("setCreateTime", Date.class);
//                setCreateTime.invoke(toObject,new Date());
//                Method setCreateBy = entityClass.getMethod("setCreateBy",String.class);
//                setCreateBy.invoke(toObject,user.getUserName());
            }
            //如果是修改方法，自动匹配修改人和修改时间
            if(methodName.contains(MyConstants.UPDATE_OPERATION)){
                ReflectUtil.invoke(toObject,"setUpdateTime", LocalDateTime.now());
                ReflectUtil.invoke(toObject,"setUpdateBy",user.getUserName());
//                Method setUpdateTime = entityClass.getMethod("setUpdateTime",Date.class);
//                setUpdateTime.invoke(toObject,new Date());
//                Method setUpdateBy = entityClass.getMethod("setUpdateBy",String.class);
//                setUpdateBy.invoke(toObject,user.getUserName());
            }
            //将参数修改完之后，再重新设置回去
            joinPointArgs[i]=toObject;
        }
        return joinPoint.proceed(joinPointArgs);
    }


}
