package com.aaa.demo.common;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;


// 演示例子，执行 main 方法控制台输入模块表名回车自动生成对应项目目录中
public class CodeGenerator {


    public static void main(String[] args) {
        //1. 全局配置
        GlobalConfig config = new GlobalConfig();
        config.setActiveRecord(true) // 是否支持AR模式
                .setAuthor("Mr.2135") // 作者
                .setOutputDir("E:\\work\\yzk\\src\\main\\java") // 生成路径
                .setFileOverride(true)// 文件覆盖
                .setOpen(false)  //是否打开输出目录
                .setSwagger2(false)//是否打开swagger
                .setIdType(IdType.NONE) // 主键策略
                .setEnableCache(false) //是否开启二级缓存
                .setServiceName("%sService")  // 设置生成的service接口的名字的首字母是否为I
                // IEmployeeService
                .setBaseResultMap(true)
                .setBaseColumnList(true);

        //2. 数据源配置
        DataSourceConfig dsConfig = new DataSourceConfig();
        dsConfig.setDbType(DbType.MYSQL)  // 设置数据库类型
                .setDriverName("com.mysql.cj.jdbc.Driver")
                .setUrl("jdbc:mysql://localhost:3306/teacher?useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=" +
                        "Hongkong&allowPublicKeyRetrieval=true")
                .setUsername("root")
                .setPassword("admin");

        //3. 策略配置
        StrategyConfig stConfig = new StrategyConfig();
        stConfig.setCapitalMode(true) //全局大写命名
                //.setDbColumnUnderline(false)  //  字段名是否使用下划线
                .setNaming(NamingStrategy.underline_to_camel) // 数据库表映指定表名射到实体的命名策略
                .setTablePrefix("tbl_")
                .setEntityTableFieldAnnotationEnable(true)//是否生成字段注解
                .setEntityLombokModel(true)//是否设置为lombok模型
                ;  // 生成的表


        //4. 包名策略配置
        PackageConfig pkConfig = new PackageConfig();
        pkConfig.setParent("com.aaa.demo")
                .setMapper("mapper")
                .setService("service")
                .setEntity("entity")
                .setXml("mapper")
                .setController("controller")
                .setServiceImpl("service.impl");

        /*//4. 包名策略配置
        PackageConfig pkConfig = new PackageConfig();
        pkConfig.setParent("com.boeyu.boeyucbsys")
                .setMapper("null")
                .setService("null")
                .setEntity("null")
                .setXml("null")
                .setController("null")
                .setServiceImpl("null");*/

        //5. 整合配置
        AutoGenerator ag = new AutoGenerator();

        ag.setGlobalConfig(config)
                .setDataSource(dsConfig)
                .setStrategy(stConfig)
                .setPackageInfo(pkConfig).setTemplateEngine(new FreemarkerTemplateEngine());

        //6. 执行
        ag.execute();
    }

}