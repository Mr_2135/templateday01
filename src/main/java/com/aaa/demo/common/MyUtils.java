package com.aaa.demo.common;

import com.aaa.demo.entity.resultmodel.PageBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Slf4j
public class MyUtils {

    // 定义jackson对象
    private static final ObjectMapper MAPPER = new ObjectMapper();




    //Map转Object
    public static Object mapToObject(Map<Object, Object> map, Class<?> beanClass) throws Exception {
        if (map == null)
            return null;
        Object obj = beanClass.newInstance();
        Field[] fields = obj.getClass().getDeclaredFields();
        for (Field field : fields) {
            int mod = field.getModifiers();
            if (Modifier.isStatic(mod) || Modifier.isFinal(mod)) {
                continue;
            }
            field.setAccessible(true);
            if (map.containsKey(field.getName())) {
                field.set(obj, map.get(field.getName()));
            }
        }
        return obj;
    }

    /**
     * 根据使用月份计算用户到期时间
     * @param useNum
     * @return
     */
   public static  String getEndTime(Integer useNum){
       Calendar cale = Calendar.getInstance();
       int month = cale.get(Calendar.MONTH) + 1;
       int day = cale.get(Calendar.DATE);
       int year = cale.get(Calendar.YEAR);

       year=year+(useNum+month)/12;
       month=(useNum+month)%12;
       String str=year+"-"+month+"-"+day+" 00:00:00";
       return str;
   }


    /**
     * 闰年的条件(满足之一即可):(1)能被4整除,但不能被100整除;(2)能被400整除
     * @param year
     * @param month
     * @return 返回天数
     */
    public static int getDays(int year, int month) {
        int days = 0;
        boolean isLeapYear = false;
        if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
            isLeapYear = true;
        } else {
            isLeapYear = false;
        }
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                days = 31;
                break;
            case 2:
                if (isLeapYear) {
                    days = 29;
                } else {
                    days = 28;
                }
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                days = 30;
                break;
            default:
                System.out.println("error!!!");
                break;
        }
        return days;
    }





    public static String getStringTime(Long time, int num) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String parse = null;//时间
        if(num<0){
            return "数据参数错误，num不可小于0";
        }
        if (num == 7) {
            try {
                parse = format.format(time);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            parse = format.format(time - (86400000 * (7 - num)));
        }
        return parse;
    }


    /**
     * 获取map集合中value的值(obj ,list)
     *
     * @param map MAP集合
     * @return List<Object>
     */
    public static List<Object> getValueFormMap(String key, Map<String, Object> map) {

        List<Object> lisMap = new LinkedList<>();

        String str = map.get(key).toString();
        log.info(str);
        return lisMap;
    }


    /**
     * 转换为json数据并返回前端
     *
     * @param response
     * @param json
     * @throws Exception
     */
    public static void returnJson(HttpServletResponse response, String json) throws Exception {
        PrintWriter writer = null;
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=utf-8");
        try {
            writer = response.getWriter();
            writer.print(json);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null)
                writer.close();
        }
    }





    /**
     * 获取分页实体类,以及每页的初始值
     *
     * @param page     页码
     * @param pageSize 每页条数
     * @param count    总条数
     * @return map
     */
    public static Map<String, Object> getPageInfo(Integer page, Integer pageSize, int count) {

        Map<String, Object> map = new HashMap<>();
        int start;
        //如果传入页码为空，默认设置为1
        if (page == null || page == 0) {
            page = 1;
        }
        if (pageSize == null || pageSize == 0) {
            pageSize = 10;
        }
        //page对象
        PageBean pageBean = new PageBean();
        pageBean.setCount(count);
        pageBean.setPageNum(page);
        pageBean.setPageSize(pageSize);
        //开始页码
        if (page == 0) {
            start = page * pageSize;
        } else {
            start = (page - 1) * pageSize;
        }
        map.put("pageBean", pageBean);
        map.put("start", start);
        return map;
    }


    public static int getStart(Integer page, Integer pageSize, int count) {
        int start;
        //如果传入页码为空，默认设置为1
        if (page.equals(null) || page == 0) {
            page = 1;
        }
        if (pageSize.equals(null) || pageSize == 0) {
            pageSize = 10;
        }
        //开始页码
        if (page == 0) {
            start = page * pageSize;

        } else {
            start = (page - 1) * pageSize;

        }
        return start;
    }


    /**
     * 将对象转换成json字符串。
     * <p>Title: pojoToJson</p>
     * <p>Description: </p>
     *
     * @param data
     * @return
     */
    public static String objectToJson(Object data) {
        if (data.equals("") || data == "") {
            return null;
        }
        try {
            String string = MAPPER.writeValueAsString(data);
            return string;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 自定义拆分字符串,返回set集合
     *
     * @param str
     * @return
     */
    public static Set<Integer> splitStr(String str) {
        char[] chars = str.toCharArray();
        StringBuffer sb = new StringBuffer();
        for (int i = 1; i < chars.length - 1; i++) {
            sb.append(chars[i]);
        }
        String[] split = sb.toString().trim().split(",");
        int[] ints = new int[split.length];
        for (int i = 0; i < split.length; i++) {
            ints[i] = Integer.parseInt(split[i]);
        }
        Arrays.sort(ints);
        //把数组放入set集合去重
        Set<Integer> numSet = new LinkedHashSet<>();
        for (int i = 0; i < ints.length; i++) {
            numSet.add(ints[i]);
        }
        return numSet;
    }

    /**
     * 用“,”拆分字符串，返回List<String>
     *
     * @param str
     * @return
     */
    public static Set<String> splitWords(String str) {
        char[] chars = str.toCharArray();
        StringBuffer sb = new StringBuffer();
        for (int i = 1; i < chars.length - 1; i++) {
            sb.append(chars[i]);
        }
        String[] split = sb.toString().split(",");
        /*String[] split = str.split(",");*/
        Set<String> Set = new LinkedHashSet<>();
        for (int i = 0; i < split.length; i++) {
            log.info(split[i]);
            Set.add(split[i].replace("\"", "").trim());
        }
        return Set;
    }





    /**
     * 是否是Ajax异步请求
     */
    public static boolean isAjaxRequest(HttpServletRequest request) {

        String accept = request.getHeader("accept");
        if (accept != null && accept.indexOf("application/json") != -1) {
            return true;
        }

        String xRequestedWith = request.getHeader("X-Requested-With");
        if (xRequestedWith != null && xRequestedWith.indexOf("XMLHttpRequest") != -1) {
            return true;
        }

       /* String uri = request.getRequestURI();
        if (StringUtils.inStringIgnoreCase(uri, ".json", ".xml"))
        {
            return true;
        }

        String ajax = request.getParameter("__ajax");
        if (StringUtils.inStringIgnoreCase(ajax, "json", "xml"))
        {
            return true;
        }*/

        return false;
    }


    /**
     * 通过IP地址获取MAC地址
     *
     * @param ip String,127.0.0.1格式
     * @return mac String
     * @throws Exception
     */
    public static String getMACAddress(String ip) throws Exception {
        String line = "";
        String macAddress = "";
        final String MAC_ADDRESS_PREFIX = "MAC Address = ";
        final String LOOPBACK_ADDRESS = "127.0.0.1";
        //如果为127.0.0.1,则获取本地MAC地址。
        if (LOOPBACK_ADDRESS.equals(ip)) {
            InetAddress inetAddress = InetAddress.getLocalHost();
            //貌似此方法需要JDK1.6。
            byte[] mac = NetworkInterface.getByInetAddress(inetAddress).getHardwareAddress();
            //下面代码是把mac地址拼装成String
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mac.length; i++) {
                if (i != 0) {
                    sb.append("-");
                }
                //mac[i] & 0xFF 是为了把byte转化为正整数
                String s = Integer.toHexString(mac[i] & 0xFF);
                sb.append(s.length() == 1 ? 0 + s : s);
            }
            //把字符串所有小写字母改为大写成为正规的mac地址并返回
            macAddress = sb.toString().trim().toUpperCase();
            return macAddress;
        }
        //获取非本地IP的MAC地址
        try {
            Process p = Runtime.getRuntime().exec("nbtstat -A " + ip);
            InputStreamReader isr = new InputStreamReader(p.getInputStream());
            BufferedReader br = new BufferedReader(isr);
            while ((line = br.readLine()) != null) {
                if (line != null) {
                    int index = line.indexOf(MAC_ADDRESS_PREFIX);
                    if (index != -1) {
                        macAddress = line.substring(index + MAC_ADDRESS_PREFIX.length()).trim().toUpperCase();
                    }
                }
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
        return macAddress;
    }


    /**
     * 读取文件内容 转换为字符串
     *
     * @param path
     * @return
     */
    public static List<String> readFIleToString(String path) {
        File file = new File(path);
        FileInputStream is;
        /* StringBuilder stringBuilder=null;*/
        List<String> stringList = new LinkedList<>();
        try {
            if (file.length() != 0) {
                is = new FileInputStream(file);
                InputStreamReader streamReader = new InputStreamReader(is);
                BufferedReader reader = new BufferedReader(streamReader);
                String line;
                /*stringBuilder = new StringBuilder();*/
                while ((line = reader.readLine()) != null) {
                    String[] split = line.split(",");
                    stringList.add(split[0].replace("\"", ""));
                    // stringBuilder.append(line);
                }
                reader.close();
                is.close();
            } else {
                log.info("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringList;
    }


    /**
     * 获取随机数组
     *
     * @param maxListSize 最大值
     * @param needSize    组要的数组长度
     * @return
     */
    public static List<Integer> getRandomNum(int maxListSize, int needSize) {
        List<Integer> nums = new ArrayList<>();
        for (int i = 0; i < needSize; i++) {
            int result = (int) (Math.random() * maxListSize);
            nums.add(result);
            log.info(result + "");
        }
        return nums;
    }

    /**
     * 通用,上传图片，文件
     *
     * @param file
     * @return
     */
    public static String uploadImg(MultipartFile file, Object id, String path) {
        String name = file.getOriginalFilename();
        String filename;
        if (name != null) {
            String[] split = name.split("\\.");
            filename = id + "." + split[1];
        } else {
            filename = "wrong";
        }

        File newFile = new File(path, filename);

        try {
            file.transferTo(newFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return filename;
    }


    public static File multipartFileToFile(MultipartFile file) throws Exception {

        File toFile = null;
        if (file.equals("") || file.getSize() <= 0) {
            file = null;
        } else {
            InputStream ins = null;
            ins = file.getInputStream();
            toFile = new File(file.getOriginalFilename());
            inputStreamToFile(ins, toFile);
            ins.close();
        }
        return toFile;
    }


    //获取流文件
    private static void inputStreamToFile(InputStream ins, File file) {
        try {
            OutputStream os = new FileOutputStream(file);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.close();
            ins.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除本地临时文件
     *
     * @param file
     */
    public static void delteTempFile(File file) {
        if (file != null) {
            File del = new File(file.toURI());
            del.delete();
        }

    }


    /**
     * 字符串 转txt文件
     *
     * @param str  json字符串
     * @param path 生成文件路径
     */
    public static void stringToFile(String str, String path) {
        File file = new File(path);
        try {
            if (!file.exists()) {
                file.createNewFile();
            } else {
                file.delete();
            }
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(path)),
                    StandardCharsets.UTF_8));
            out.write(str);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




    /**
     * 压缩文件
     *
     * @param filePath 压缩文件带路径
     * @param zipPath  压缩后的文件路径
     */
    public static void fileToZip(String filePath, String zipPath) {

        File file = new File(filePath);
        if (!file.exists()) {
            throw new RuntimeException("文件不存在");
        }
        File zipFile = new File(zipPath);
        try {

            FileOutputStream fos = new FileOutputStream(zipFile);
            ZipOutputStream zos = new ZipOutputStream(fos);
            String baseDir = "";
            compressByType(file, zos, baseDir);
            zos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 按照原路径的类型进行压缩。文件路径直接把文件压缩，
     *
     * @param src     需要压缩的文件
     * @param zos     压缩流
     * @param baseDir 文件上级目录
     */
    private static void compressByType(File src, ZipOutputStream zos, String baseDir) {

        if (!src.exists())
            return;
        System.out.println("压缩路径" + baseDir + src.getName());
        //判断文件是否是文件，如果是文件调用compressFile方法,如果是路径，则调用compressDir方法；
        if (src.isFile()) {
            //src是文件，调用此方法
            compressFile(src, zos, baseDir);

        } else if (src.isDirectory()) {
            //src是文件夹，调用此方法
            compressDir(src, zos, baseDir);

        }

    }


    /**
     * 压缩文件
     */
    private static void compressFile(File file, ZipOutputStream zos, String baseDir) {
        if (!file.exists())
            return;
        try {
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
            ZipEntry entry = new ZipEntry(baseDir + file.getName());
            zos.putNextEntry(entry);
            int count;
            byte[] buf = new byte[1024];
            while ((count = bis.read(buf)) != -1) {
                zos.write(buf, 0, count);
            }
            bis.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 压缩文件夹
     */
    private static void compressDir(File dir, ZipOutputStream zos, String baseDir) {
        if (!dir.exists())
            return;
        File[] files = dir.listFiles();
        if (files.length == 0) {
            try {
                zos.putNextEntry(new ZipEntry(baseDir + dir.getName() + File.separator));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        for (File file : files) {
            compressByType(file, zos, baseDir + dir.getName() + File.separator);
        }
    }
    private static final String UNKNOWN = "unknown";
    /**
     * 获取ip地址
     */
    public static String getIp(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        String comma = ",";
        String localhost = "127.0.0.1";
        if (ip.contains(comma)) {
            ip = ip.split(",")[0];
        }
        if (localhost.equals(ip)) {
            // 获取本机真正的ip地址
            try {
                ip = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
                log.error(e.getMessage(), e);
            }
        }
        return ip;
    }




/*
    public static void main(String[] args) {

        String s = RandomUtils.generateMixString(3) + System.currentTimeMillis();
        log.info(s);
        String substring = s.substring(10, s.length() - 1).toString();



    }
*/


}
