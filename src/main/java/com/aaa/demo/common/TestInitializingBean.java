package com.aaa.demo.common;

import cn.hutool.setting.dialect.Props;
import com.aaa.demo.mapper.TblMenuMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @description: 项目启动时初始化数据
 * @author: Mr.2135
 * @date: 2021-11-13 10:12
 */
@Component
@Slf4j
public class TestInitializingBean implements InitializingBean {

    @Autowired
    private TblMenuMapper menuMapper;

    @Override
    public void afterPropertiesSet(){
//        Props props = new Props("application.properties");
//        props.setProperty("integral.learn",1);
//        props.setProperty("integral.most-rewards",50);
//        log.info("初始化数据：{}",props.getStr("integral.learn"));
//        log.info("初始化数据：{}",props.getStr("integral.most-rewards"));
    }
}


