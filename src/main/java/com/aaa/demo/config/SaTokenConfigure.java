//package com.aaa.demo.config;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
///**
// * @description: [Sa-Token 权限认证] 配置类
// * @author: Mr.2135
// * @date: 2021-05-22 17:35
// */
//@Configuration
//public class SaTokenConfigure implements WebMvcConfigurer {
//
//    // 注册拦截器
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        // 注册Sa-Token的路由拦截器
//        registry.addInterceptor(new SaTokenLoginInterceptor())
//                .addPathPatterns("/**")
//                .excludePathPatterns("/user/login",
//                        "/user/captcha",
//                        "/user/mpLogin",
//                        "/wxLogin/decodeUserInfo");
//    }
//}
