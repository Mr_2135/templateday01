package com.aaa.demo.config;

import cn.dev33.satoken.stp.StpInterface;
import com.aaa.demo.entity.TblMenu;
import com.aaa.demo.mapper.TblUserMapper;
import com.aaa.demo.redis.RedisCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: Administrator
 * @date: 2021-05-22 17:54
 */
@Component
@Slf4j
public class StpInterfaceImpl implements StpInterface
{
    @Autowired
    private TblUserMapper userMapper;

    @Autowired
    private RedisCache redisCache;

    @Override
    public List<String> getPermissionList(Object loginId, String loginKey) {
        List<String> list = new ArrayList<>();
        if ("login".equals(loginKey)){
            List<TblMenu> menus = userMapper.selectMenuByUserId(Integer.valueOf(loginId.toString()));
            for (TblMenu menu : menus) {
                list.add(menu.getPerms());
            }
            return list;
        }
        return null;
    }

    @Override
    public List<String> getRoleList(Object o, String s) {
        return null;
    }
}
