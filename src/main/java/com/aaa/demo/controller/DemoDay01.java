package com.aaa.demo.controller;

/**
 * @description: 联系多线程
 * @author: Mr.wang
 * @date: 2021-08-12 16:08
 */
public class DemoDay01 implements Runnable {
    private int ticket = 5 ;    // 假设一共有5张票

    @Override
    public void run(){
        for(int i=0;i<100;i++){
            synchronized(this){ // 要对当前对象进行同步
                if(ticket>0){   // 还有票
                    System.out.println(Thread.currentThread().getName()+":卖票：ticket = " + ticket-- );

                }
            }
        }
    }

    public static void main(String[] args) {
       DemoDay01 day01 = new DemoDay01();
       new Thread(day01,"线程A").start();
       new Thread(day01,"线程B").start();
    }
}
