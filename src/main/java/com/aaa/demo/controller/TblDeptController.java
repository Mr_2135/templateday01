package com.aaa.demo.controller;


import cn.dev33.satoken.stp.StpUtil;
import com.aaa.demo.aop.SaveOrUpdateEntityAnn;
import com.aaa.demo.core.controller.BaseController;
import com.aaa.demo.entity.TblDept;
import com.aaa.demo.entity.page.PageDomain;
import com.aaa.demo.entity.page.TableDataInfo;
import com.aaa.demo.enums.GradeEnum;
import com.aaa.demo.service.TblDeptService;
import com.aaa.demo.entity.resultmodel.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 部门表 前端控制器
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
@RestController
@RequestMapping("/tblDept")
public class TblDeptController {
    @Autowired
    private TblDeptService deptService;


    @GetMapping("/deptList")
    public TableDataInfo deptList(String deptName){
        BaseController.startPage();
        List<TblDept> depts = deptService.deptList(deptName);
        return BaseController.getDataTable(depts);
    }
    @PostMapping("/save")
    @SaveOrUpdateEntityAnn(entityClass = TblDept.class,describe = "添加部门")
    public JsonResult save(TblDept dept){
//        StpUtil.checkPermission("system:dept:add");
        return deptService.deptAdd(dept);
    }
    @PutMapping("/updateDept")
    @SaveOrUpdateEntityAnn(entityClass = TblDept.class,describe = "修改部门")
    public JsonResult updateDept(TblDept dept){
        return deptService.deptUpdate(dept);
    }


}
