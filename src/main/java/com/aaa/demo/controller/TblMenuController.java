package com.aaa.demo.controller;


import com.aaa.demo.service.TblMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 菜单权限表 前端控制器
 * </p>
 *
 * @author cwp
 * @since 2021-11-24
 */
@RestController
@RequestMapping("/menu")
public class TblMenuController {
    @Autowired
    private TblMenuService menuService;


}
