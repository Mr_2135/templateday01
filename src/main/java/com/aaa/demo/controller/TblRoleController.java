package com.aaa.demo.controller;


import cn.dev33.satoken.stp.StpUtil;
import com.aaa.demo.entity.TblRole;
import com.aaa.demo.service.TblRoleService;
import com.aaa.demo.entity.resultmodel.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色信息表 前端控制器
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
@RestController
@RequestMapping("/tblRole")
public class TblRoleController {
    @Autowired
    private TblRoleService roleService;

    @GetMapping("/roleList")
    public JsonResult roleList(Integer pageNum,Integer pageSize,String roleByName){
        StpUtil.checkPermission("system:role:list");
        return roleService.roleList(pageNum,pageSize,roleByName);
    }
    @GetMapping("/roleAdd")
    public JsonResult roleAdd(TblRole role, Integer[] menuId){
        StpUtil.checkPermission("system:role:add");
        return roleService.roleAdd(role,menuId);
    }

    @GetMapping("/updateRole")
    public JsonResult updateRole(TblRole role){
        StpUtil.checkPermission("system:role:edit");
        return roleService.updateRole(role);
    }

}
