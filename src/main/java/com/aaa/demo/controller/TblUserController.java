package com.aaa.demo.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 用户信息表 前端控制器
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
@Controller
@RequestMapping("/tblUser")
public class  TblUserController {

}
