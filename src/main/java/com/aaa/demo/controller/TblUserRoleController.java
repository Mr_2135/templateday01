package com.aaa.demo.controller;


import com.aaa.demo.entity.resultmodel.JsonResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 用户和角色关联表 前端控制器
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
@RestController
@RequestMapping("/tblUserRole")
public class TblUserRoleController {
    @PostMapping("/test")
    public JsonResult test(HttpServletRequest request) throws IOException {
//        Map<String,Object> map = new HashMap<>(16);
        System.out.println(request.getHeader("Authorization"));
        Enumeration<String> parameterNames = request.getParameterNames();
        System.out.println(parameterNames);
//        JSONObject object = JSONObject.fromObject(request.getInputStream());
//        ServletInputStream is;
//        try {
//            is = request.getInputStream();
//            int nRead = 1;
//            int nTotalRead = 0;
//            byte[] bytes = new byte[10240];
//            while (nRead > 0) {
//                nRead = is.read(bytes, nTotalRead, bytes.length - nTotalRead);
//                if (nRead > 0)
//                    nTotalRead = nTotalRead + nRead;
//            }
//            String str = new String(bytes, 0, nTotalRead, "utf-8");
//            System.out.println(str);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        map.put("name",login.getName());
        return JsonResult.ok("成功");
    }
}
