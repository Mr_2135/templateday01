package com.aaa.demo.controller;

import cn.dev33.satoken.session.SaSession;
import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.http.useragent.UserAgentUtil;
import com.aaa.demo.entity.TblUser;
import com.aaa.demo.entity.resultmodel.AjaxResult;
import com.aaa.demo.entity.resultmodel.LoginBody;
import com.aaa.demo.service.TblMenuService;
import com.aaa.demo.service.TblUserService;
import com.aaa.demo.entity.resultmodel.JsonResult;
import com.aaa.demo.util.ServletUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Objects;
import java.util.Set;


/**
 * @description: 用户
 * @author: Administrator
 * @date: 2021-05-22 15:06
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private TblUserService userService;
    @Autowired
    private TblMenuService menuService;

    @PostMapping("/login")
    public AjaxResult login(LoginBody loginBody){
        HttpServletRequest request = ServletUtils.getRequest();
        log.info("获取到参数：{}",request.getParameter("username"));
        return userService.login(loginBody.getUsername(),loginBody.getPassword(),loginBody.getCode(),loginBody.getUuid());
    }

    /**
     * 生成验证码
     * @return
     */
    @GetMapping("/captcha")
    public AjaxResult captcha()
    {
        return userService.captcha();
    }

    @GetMapping("/getInfo")
    public AjaxResult getInfo()
    {
        TblUser user = new TblUser(){{
            setUserId(Integer.parseInt(StpUtil.getLoginId().toString()));
        }};
        Map<String, Object> userInfo = userService.getUserInfo(user);
        Set<String> menu = menuService.selectMenuByUserId(user);
        AjaxResult success = AjaxResult.success();
        success.put("permissions",menu);
        success.put("user",userInfo);
        return success;
    }

    @GetMapping("/getRouters")
    public AjaxResult getRouters()
    {
        return menuService.getRouters(Integer.parseInt(StpUtil.getLoginId().toString()));
    }

    @GetMapping("/signUp")
    public JsonResult signUp(TblUser user){
        return null;
    }

    @GetMapping("isLogin")
    public String isLogin(String username, String password,String loginId) {
//        boolean b = StpUtil.hasPermission("system:user:list");
//        boolean c = StpUtil.hasPermission("system:dept:remove");
//        StpUtil.checkPermission("system:dept:remove");
//        int i =1/0;
        SaSession sessionByLoginId = StpUtil.getSessionByLoginId(loginId);
//        return "获取当前会话登录id：" + StpUtil.getLoginIdAsString()+"登录设备："+StpUtil.getLoginDevice();
        return sessionByLoginId.getTokenSignList().toString();
    }
    @GetMapping("mpLogin")
    public String mpLogin(String openid,String phone) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = Objects.requireNonNull(attributes).getRequest();
        String header = request.getHeader("User-Agent");
        StpUtil.setLoginId(phone, UserAgentUtil.parse(header).getOs().toString());
        log.info("登录设备：{}",StpUtil.getLoginDevice());
        SaTokenInfo tokenInfo = StpUtil.getTokenInfo();
        return tokenInfo.getTokenValue();
    }



    @GetMapping("userList")
    public JsonResult userList(Integer pageNum,Integer pageSize,String userName) {
        StpUtil.checkPermissionAnd("system:user:view","system:user:list");
        return userService.userList(pageNum, pageSize, userName);
    }
    /**
     * 获取用户的在线设备
     * @param loginId
     * @return
     */
    @GetMapping("userDevice")
    public JsonResult userDevice(String loginId) {
        SaSession userDevice = StpUtil.getSessionByLoginId(loginId);
        if (userDevice.getTokenSignList().size() > 0){
            return JsonResult.ok(userDevice.getTokenSignList());
        }
        return JsonResult.errorMsg("用户未有在线设备");
    }

    /**
     * 根据用户id获取权限
     * @param loginId
     * @return
     */
    @GetMapping("getUserMenu")
    public JsonResult getUserMenu(Integer loginId){
        return userService.getUserMenu(loginId);
    }
    /**
     * 禁用用户
     * @param userId
     * @return
     */
    @GetMapping("disable")
    public JsonResult disable(Integer userId,String disableTime) {
       return userService.disable(userId,disableTime);
    }

    /**
     * 根据token使指定用户下线
     * @param tokenValue
     * @return
     */
    @GetMapping("logoutByLoginid")
    public JsonResult logoutByLoginid(String tokenValue) {
        StpUtil.logoutByTokenValue(tokenValue);
        return JsonResult.okMsg("用户已下线");
    }

    @PostMapping("logout")
    public void logout() {
        StpUtil.logout();

    }

    /**
     *
     * @param password
     * @param username
     * @param time
     * @return
     */

    @GetMapping("/decryptStr")
    public JsonResult decryptStr(String password,String username,String time) {
        log.info("password:{},username:{},time:{}",password,username,time);
        return JsonResult.okMsg("成功");
    }
}
