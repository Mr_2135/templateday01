package com.aaa.demo.controller;


import cn.dev33.satoken.secure.SaSecureUtil;
import cn.dev33.satoken.stp.StpUtil;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
@Controller
@RequestMapping("/userinfo")
public class UserinfoController {
    public static void main(String[] args) {
        String s = SaSecureUtil.md5BySalt("123456", "4bd21f");
        System.out.println(s);
    }

}
