package com.aaa.demo.controller;

import com.aaa.demo.service.impl.WebSocketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author: Mr.2135
 * @date: 2022-04-16 12:13
 */
@RestController
public class WebSocketController {
    @Autowired
    private WebSocketService webSocketService;

    @GetMapping("/sendToMessage")
    public String sendToMessage(String message, String toDeviceId)
    {
        webSocketService.sendMessageTo(message, toDeviceId);
        return "推送成功";
    }
}
