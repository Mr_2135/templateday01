package com.aaa.demo.controller;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.aaa.demo.entity.resultmodel.JsonResult;
import com.aaa.demo.util.AesCbcUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @description:
 * @author: Administrator
 * @date: 2021-10-30 10:30
 */
@Controller
@RequestMapping("/wxLogin")
@Slf4j
public class WxLoginController {

    /**
     * 解密用户敏感数据
     *@RequestParam("encryptedData") String encryptedData,
     *                                      @RequestParam("iv") String iv,
     * 明文,加密数据
     * 加密算法的初始向量
     * @param codeId          用户允许登录后，回调内容会带上 code（有效期五分钟），开发者需要将 code 发送到开发者服务器后台，使用code 换取 session_key api，将 code 换成 openid 和 session_key
     * @return
     */
    @ResponseBody
    @PostMapping(value = "/decodeUserInfo")
    public JsonResult decodeUserInfo(@RequestParam("codeId") String codeId,
                                    @RequestParam("encryptedData") String encryptedData,
                                           @RequestParam("iv") String iv
    ) {
        if (StringUtils.isEmpty(codeId)){
            return JsonResult.errorMsg("授权失败");
        }
        Map<String,Object> map = new HashMap<>(16);
        //小程序唯一标识   (在微信小程序管理后台获取)
        String wxspAppid = "wx2305785096cf32fb";
        //小程序的 app secret (在微信小程序管理后台获取)
        String wxspSecret = "d039eaa71893893308d2c77d6e43efd1";
        //授权（必填）
        String grantType = "authorization_code";
//        1、向微信服务器 使用登录凭证 code 获取 session_key 和 openid
        //请求参数
        map.put("appid",wxspAppid);
        map.put("secret",wxspSecret);
        map.put("js_code",codeId);
        map.put("grant_type",grantType);
//        //发送请求
        String params = HttpUtil.get("https://api.weixin.qq.com/sns/jscode2session",map);
        log.info("返回的参数：{}",params);
        JSONObject json = JSONUtil.parseObj(params);
        //获取会话密钥（session_key）
        String sessionKey = json.get("session_key").toString();
        //用户的唯一标识（openid）
        String openid = json.get("openid").toString();
//        2、对encryptedData加密数据进行AES解密
//        if (StringUtils.isNotEmpty(openid)){
//            Map<String,String> userInfo = new HashMap<>(16);
//            StpUtil.setLoginId(openid);
//            userInfo.put("openId", openid);
//            userInfo.put("sessionKey", sessionKey);
//            userInfo.put("satoken",StpUtil.getTokenValue());
//            return JsonResult.ok(userInfo);
//        }
        try {
            String result = AesCbcUtil.decrypt(encryptedData, sessionKey, iv);
            if (StringUtils.isNotEmpty(result)) {
                log.info("解密的数据：{}",result);
                JSONObject userInfoJson = JSONUtil.parseObj(result);
                Map<String,String> userInfo = new HashMap<>(16);
                userInfo.put("openId", openid);
                userInfo.put("nickName", userInfoJson.getStr("nickName"));
                userInfo.put("gender", userInfoJson.getStr("gender"));
                userInfo.put("city", userInfoJson.getStr("city"));
                userInfo.put("province", userInfoJson.getStr("province"));
                userInfo.put("country", userInfoJson.getStr("country"));
                userInfo.put("avatarUrl", userInfoJson.getStr("avatarUrl"));
                userInfo.put("unionId", userInfoJson.getStr("unionId"));
                return JsonResult.ok(userInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JsonResult.errorMsg("获取用户信息失败");
    }


}
