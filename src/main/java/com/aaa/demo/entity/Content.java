package com.aaa.demo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 信息发布
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_content")
public class Content extends Model<Content> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 名称
     */
    @TableField("content_name")
    private String contentName;

    /**
     * 作者
     */
    @TableField("author")
    private String author;

    /**
     * 来源
     */
    @TableField("source")
    private String source;

    /**
     * 描述
     */
    @TableField("describ")
    private String describ;

    /**
     * 详情
     */
    @TableField("details")
    private String details;

    /**
     * 浏览次数
     */
    @TableField("browse")
    private Integer browse;

    /**
     * 封面
     */
    @TableField("img")
    private String img;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 父id
     */
    @TableField("release_id")
    private Integer releaseId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
