package com.aaa.demo.entity;

import lombok.Data;

/**
 * @description:
 * @author: Administrator
 * @date: 2021-06-24 11:40
 */
@Data
public class Loginlog {
    //编号
    private String loginId;
    //登录名称
    private String loginName;
    // ip
    private String ip;
    // 登录地址
    private String loginAddress;
    // 浏览器
    private String browser;
    // 操作系统
    private String os;
    // 操作信息
    private String information;
    // 时间
    private String loginDate;


}
