package com.aaa.demo.entity.resultmodel;

/**
 * @ClassName: Constants
 * @Description: 公共常量类
 * @author 谭红波
 * @date 2018年3月2日
 * 
 */
public final class Constants {
    public static final String USER_FAILUE = "用户操作异常";

    public static final String QUERY_SUCCESS = "查询成功";
    public static final String QUERY_FAILUE = "查询失败";

    public static final String INSERT_SUCCESS = "新增成功";
    public static final String INSERT_FAILUE = "新增失败";

    public static final String UPDATE_SUCCESS = "修改成功";
    public static final String UPDATE_FAILUE = "修改失败";

    public static final String DELETE_SUCCESS = "删除成功";
    public static final String DELETE_FAILUE = "删除失败";

    public static final String EXPORT_SUCCESS = "导出成功";
    public static final String EXPORT_FAILUE = "导出失败";

    public static final String FOCUS_SUCCESS = "关注成功";
    public static final String FOCUS_FAILUE = "关注失败";

    public static final String CANCEL_FOCUS_SUCCESS = "取消关注成功";
    public static final String CANCEL_FOCUS_FAILUE = "取消关注失败";

    public static final String DEAL_SUCCESS = "操作成功";
    public static final String DEAL_FAILUE = "操作失败";

    public static final String SHARE_SUCCESS = "共享成功";
    public static final String SHARE_FAILUE = "共享失败";

    public static final String FEEK_SUCCESS = "反馈成功";
    public static final String FEEK_FAILUE = "反馈失败";

    public static final String FILE_SUCCESS = "上传成功";
    public static final String FILE_FAILUE = "上传失败";
    
    public static final String DATA_ISNULL = "数据为空";
    
    public static final String BEFORE_INTEGRATE_DATA_ISNULL = "未过集成数据时间不正确";
    public static final String AFTER_INTEGRATE_DATA_ISNULL = "已过集成数据时间不正确";
}
