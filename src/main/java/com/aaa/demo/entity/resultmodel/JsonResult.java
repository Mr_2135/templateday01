package com.aaa.demo.entity.resultmodel;/**
 * Created by CuiWeiPeng on 2019/7/4
 */

import com.aaa.demo.util.MyConstants;

import java.io.Serializable;

/**
 * @ClassName JsonResult
 * @Description 自定义响应数据结构
 * 200：表示成功
 * 500：表示错误，错误信息在msg字段中
 * 501：bean验证错误，不管多少个错误都以map形式返回
 * 502：拦截器拦截到用户token出错
 * 555：异常抛出信息
 * 101:自定义错误,错误信息在msg字段中
 * @Author Cuiweipeng
 * @Date 2019/7/414:36
 * @Version 1.8
 */


public class JsonResult implements Serializable {

    private static final long serialVersionUID = 1L;
    //状态码

    private int status;
    //数据内容

    private Object data;
    //消息
    private String msg;

    public static JsonResult ok() {
        return new JsonResult(null);
    }

    public static JsonResult ok(Object data) {
        return new JsonResult(data);
    }

    public static JsonResult ok(int status,Object data) {
        return new JsonResult(status,data);
    }

    public static JsonResult ok(Object data, String msg) {
        return new JsonResult(data, msg);
    }

    public static JsonResult okMsg(String msg) {
        return new JsonResult( msg);
    }

    public static JsonResult okMsgData(String msg, Object data) {
        return new JsonResult( data, msg);
    }

    public static JsonResult errorMsgAndCode(int status, String msg) {
        return new JsonResult(status, null, msg);
    }

    public static JsonResult errorMsg(String msg) {
        return new JsonResult(500, null, msg);
    }

    public static JsonResult errorMap(Object data) {
        return new JsonResult(501, data, "error");
    }

    public static JsonResult errorTokenMsg(String msg) {
        return new JsonResult(502, null, msg);
    }

    public static JsonResult errorException(String msg) {
        return new JsonResult(555, null, msg);
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public JsonResult(int status, Object data, String msg) {
        this.status = status;
        this.data = data;
        this.msg = msg;
    }

    public JsonResult(Object data, String msg) {
        this.status = 200;
        this.data = data;
        this.msg = msg;
    }

    public JsonResult(int status, Object data) {
        this.status = status;
        this.data = data;
    }

    public JsonResult(Object data) {
        this.status = 200;
        this.msg = "ok";
        this.data = data;
    }
    public JsonResult(String msg) {
        this.status = 200;
        this.msg = msg;
        this.data = null;
    }


    public JsonResult() {
    }

    /**
     * 响应返回结果
     *
     * @param rows 影响行数
     * @return 操作结果
     */
    public static JsonResult toAjax(int rows)
    {
        return rows > 0 ? JsonResult.okMsg(MyConstants.OPERATION_SUCCESS_MESSAGE) : JsonResult.errorMsg(MyConstants.OPERATION_FAIL_MESSAGE);
    }
}
