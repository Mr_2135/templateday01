package com.aaa.demo.entity.resultmodel;/**
 * Created by CuiWeiPeng on 2019/6/27
 */

import java.io.Serializable;

/**
 *@ClassName PageBean
 *@Description
 *@Author Cuiweipeng
 *@Date 2019/6/2710:53
 *@Version
 */
public class PageBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private int pageNum;//頁碼

    private int pageSize;//每頁显示条数

    private int  pageCount;//总页数

    private int count;//总条数

    private int lastPage;//最后一页

    private boolean  isLastPage;//最后一页

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageCount() {
        pageCount = count%pageSize == 0?count/pageSize :count/pageSize+1;
        return pageCount ;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isLastPage() {
        isLastPage = pageNum == lastPage;
        return isLastPage;
    }

    public int getLastPage() {
        return lastPage;
    }

    public PageBean(int pageNum, int pageSize, int pageCount, int count) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.pageCount = pageCount;
        this.count = count;
    }

    public PageBean(int pageNum, int pageSize, int count) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.count = count;
    }

    public PageBean(int pageNum, int pageSize) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }

    public PageBean() {
    }
}
