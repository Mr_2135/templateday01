package com.aaa.demo.entity.resultmodel;

/**
 * 返回的消息
 * @author Cold
 *
 */
public class ResultConstants {
	
	public static final String NO_DATA = "无数据！";
	
	public static final String EMPTY_PARAMETER = "参数不能为空！";
	
	public static final String EMPTY_PARAMETER_TIME = "时间参数不能为空！";
	
	public static final String EMPTY_PARAMETER_STATION = "站点参数不能为空！";
	
	public static final String EMPTY_PARAMETER_INDEX = "监测项参数不能为空！";
	
	public static final String EMPTY_PARAMETER_QUALITY_TYPE = "质控类型参数不能为空！";
	
	public static final String EMPTY_PAGE_PARAMETER = "分页参数不能为空！";
	
	public static final String NO_SUCH_OBJECT = "数据对象不存在！";
	
	public static final String VERIFICATION_SUCCESS = "验证成功！";
	
}
