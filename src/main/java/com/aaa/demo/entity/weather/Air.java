package com.aaa.demo.entity.weather;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @description:
 * @author: Administrator
 * @date: 2022-06-22 11:02
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "time",
        "month"
})
public class Air {
    @JsonProperty("forecasttime")
    private String forecasttime;
    @JsonProperty("aqi")
    private Integer aqi;
    @JsonProperty("aq")
    private Integer aq;
    @JsonProperty("text")
    private String text;
    @JsonProperty("aqiCode")
    private String aqiCode;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("forecasttime")
    public String getForecasttime() {
        return forecasttime;
    }

    @JsonProperty("forecasttime")
    public void setForecasttime(String forecasttime) {
        this.forecasttime = forecasttime;
    }

    @JsonProperty("aqi")
    public Integer getAqi() {
        return aqi;
    }

    @JsonProperty("aqi")
    public void setAqi(Integer aqi) {
        this.aqi = aqi;
    }

    @JsonProperty("aq")
    public Integer getAq() {
        return aq;
    }

    @JsonProperty("aq")
    public void setAq(Integer aq) {
        this.aq = aq;
    }

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    @JsonProperty("aqiCode")
    public String getAqiCode() {
        return aqiCode;
    }

    @JsonProperty("aqiCode")
    public void setAqiCode(String aqiCode) {
        this.aqiCode = aqiCode;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Air.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("forecasttime");
        sb.append('=');
        sb.append(((this.forecasttime == null)?"<null>":this.forecasttime));
        sb.append(',');
        sb.append("aqi");
        sb.append('=');
        sb.append(((this.aqi == null)?"<null>":this.aqi));
        sb.append(',');
        sb.append("aq");
        sb.append('=');
        sb.append(((this.aq == null)?"<null>":this.aq));
        sb.append(',');
        sb.append("text");
        sb.append('=');
        sb.append(((this.text == null)?"<null>":this.text));
        sb.append(',');
        sb.append("aqiCode");
        sb.append('=');
        sb.append(((this.aqiCode == null)?"<null>":this.aqiCode));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.aq == null)? 0 :this.aq.hashCode()));
        result = ((result* 31)+((this.aqiCode == null)? 0 :this.aqiCode.hashCode()));
        result = ((result* 31)+((this.forecasttime == null)? 0 :this.forecasttime.hashCode()));
        result = ((result* 31)+((this.aqi == null)? 0 :this.aqi.hashCode()));
        result = ((result* 31)+((this.text == null)? 0 :this.text.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Air) == false) {
            return false;
        }
        Air rhs = ((Air) other);
        return (((((((this.aq == rhs.aq)||((this.aq!= null)&&this.aq.equals(rhs.aq)))&&((this.aqiCode == rhs.aqiCode)||((this.aqiCode!= null)&&this.aqiCode.equals(rhs.aqiCode))))&&((this.forecasttime == rhs.forecasttime)||((this.forecasttime!= null)&&this.forecasttime.equals(rhs.forecasttime))))&&((this.aqi == rhs.aqi)||((this.aqi!= null)&&this.aqi.equals(rhs.aqi))))&&((this.text == rhs.text)||((this.text!= null)&&this.text.equals(rhs.text))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))));
    }

}
