package com.aaa.demo.entity.weather;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
/**
 * @description:
 * @author: Administrator
 * @date: 2022-06-22 11:08
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "month",
        "maxTemp",
        "minTemp",
        "precipitation"
})
public class Month {

    @JsonProperty("month")
    private Integer month;
    @JsonProperty("maxTemp")
    private Double maxTemp;
    @JsonProperty("minTemp")
    private Double minTemp;
    @JsonProperty("precipitation")
    private Double precipitation;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("month")
    public Integer getMonth() {
        return month;
    }

    @JsonProperty("month")
    public void setMonth(Integer month) {
        this.month = month;
    }

    @JsonProperty("maxTemp")
    public Double getMaxTemp() {
        return maxTemp;
    }

    @JsonProperty("maxTemp")
    public void setMaxTemp(Double maxTemp) {
        this.maxTemp = maxTemp;
    }

    @JsonProperty("minTemp")
    public Double getMinTemp() {
        return minTemp;
    }

    @JsonProperty("minTemp")
    public void setMinTemp(Double minTemp) {
        this.minTemp = minTemp;
    }

    @JsonProperty("precipitation")
    public Double getPrecipitation() {
        return precipitation;
    }

    @JsonProperty("precipitation")
    public void setPrecipitation(Double precipitation) {
        this.precipitation = precipitation;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Month.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("month");
        sb.append('=');
        sb.append(((this.month == null)?"<null>":this.month));
        sb.append(',');
        sb.append("maxTemp");
        sb.append('=');
        sb.append(((this.maxTemp == null)?"<null>":this.maxTemp));
        sb.append(',');
        sb.append("minTemp");
        sb.append('=');
        sb.append(((this.minTemp == null)?"<null>":this.minTemp));
        sb.append(',');
        sb.append("precipitation");
        sb.append('=');
        sb.append(((this.precipitation == null)?"<null>":this.precipitation));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.maxTemp == null)? 0 :this.maxTemp.hashCode()));
        result = ((result* 31)+((this.precipitation == null)? 0 :this.precipitation.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.month == null)? 0 :this.month.hashCode()));
        result = ((result* 31)+((this.minTemp == null)? 0 :this.minTemp.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Month) == false) {
            return false;
        }
        Month rhs = ((Month) other);
        return ((((((this.maxTemp == rhs.maxTemp)||((this.maxTemp!= null)&&this.maxTemp.equals(rhs.maxTemp)))&&((this.precipitation == rhs.precipitation)||((this.precipitation!= null)&&this.precipitation.equals(rhs.precipitation))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.month == rhs.month)||((this.month!= null)&&this.month.equals(rhs.month))))&&((this.minTemp == rhs.minTemp)||((this.minTemp!= null)&&this.minTemp.equals(rhs.minTemp))));
    }

}
