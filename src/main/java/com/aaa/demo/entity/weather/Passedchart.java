package com.aaa.demo.entity.weather;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
/**
 * @description:
 * @author: Administrator
 * @date: 2022-06-22 11:10
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "rain1h",
        "rain24h",
        "rain12h",
        "rain6h",
        "temperature",
        "tempDiff",
        "humidity",
        "pressure",
        "windDirection",
        "windSpeed",
        "time"
})
public class Passedchart {
    @JsonProperty("rain1h")
    private Integer rain1h;
    @JsonProperty("rain24h")
    private Integer rain24h;
    @JsonProperty("rain12h")
    private Integer rain12h;
    @JsonProperty("rain6h")
    private Integer rain6h;
    @JsonProperty("temperature")
    private Double temperature;
    @JsonProperty("tempDiff")
    private String tempDiff;
    @JsonProperty("humidity")
    private Integer humidity;
    @JsonProperty("pressure")
    private Integer pressure;
    @JsonProperty("windDirection")
    private Integer windDirection;
    @JsonProperty("windSpeed")
    private Double windSpeed;
    @JsonProperty("time")
    private String time;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("rain1h")
    public Integer getRain1h() {
        return rain1h;
    }

    @JsonProperty("rain1h")
    public void setRain1h(Integer rain1h) {
        this.rain1h = rain1h;
    }

    @JsonProperty("rain24h")
    public Integer getRain24h() {
        return rain24h;
    }

    @JsonProperty("rain24h")
    public void setRain24h(Integer rain24h) {
        this.rain24h = rain24h;
    }

    @JsonProperty("rain12h")
    public Integer getRain12h() {
        return rain12h;
    }

    @JsonProperty("rain12h")
    public void setRain12h(Integer rain12h) {
        this.rain12h = rain12h;
    }

    @JsonProperty("rain6h")
    public Integer getRain6h() {
        return rain6h;
    }

    @JsonProperty("rain6h")
    public void setRain6h(Integer rain6h) {
        this.rain6h = rain6h;
    }

    @JsonProperty("temperature")
    public Double getTemperature() {
        return temperature;
    }

    @JsonProperty("temperature")
    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    @JsonProperty("tempDiff")
    public String getTempDiff() {
        return tempDiff;
    }

    @JsonProperty("tempDiff")
    public void setTempDiff(String tempDiff) {
        this.tempDiff = tempDiff;
    }

    @JsonProperty("humidity")
    public Integer getHumidity() {
        return humidity;
    }

    @JsonProperty("humidity")
    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }

    @JsonProperty("pressure")
    public Integer getPressure() {
        return pressure;
    }

    @JsonProperty("pressure")
    public void setPressure(Integer pressure) {
        this.pressure = pressure;
    }

    @JsonProperty("windDirection")
    public Integer getWindDirection() {
        return windDirection;
    }

    @JsonProperty("windDirection")
    public void setWindDirection(Integer windDirection) {
        this.windDirection = windDirection;
    }

    @JsonProperty("windSpeed")
    public Double getWindSpeed() {
        return windSpeed;
    }

    @JsonProperty("windSpeed")
    public void setWindSpeed(Double windSpeed) {
        this.windSpeed = windSpeed;
    }

    @JsonProperty("time")
    public String getTime() {
        return time;
    }

    @JsonProperty("time")
    public void setTime(String time) {
        this.time = time;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Passedchart.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("rain1h");
        sb.append('=');
        sb.append(((this.rain1h == null)?"<null>":this.rain1h));
        sb.append(',');
        sb.append("rain24h");
        sb.append('=');
        sb.append(((this.rain24h == null)?"<null>":this.rain24h));
        sb.append(',');
        sb.append("rain12h");
        sb.append('=');
        sb.append(((this.rain12h == null)?"<null>":this.rain12h));
        sb.append(',');
        sb.append("rain6h");
        sb.append('=');
        sb.append(((this.rain6h == null)?"<null>":this.rain6h));
        sb.append(',');
        sb.append("temperature");
        sb.append('=');
        sb.append(((this.temperature == null)?"<null>":this.temperature));
        sb.append(',');
        sb.append("tempDiff");
        sb.append('=');
        sb.append(((this.tempDiff == null)?"<null>":this.tempDiff));
        sb.append(',');
        sb.append("humidity");
        sb.append('=');
        sb.append(((this.humidity == null)?"<null>":this.humidity));
        sb.append(',');
        sb.append("pressure");
        sb.append('=');
        sb.append(((this.pressure == null)?"<null>":this.pressure));
        sb.append(',');
        sb.append("windDirection");
        sb.append('=');
        sb.append(((this.windDirection == null)?"<null>":this.windDirection));
        sb.append(',');
        sb.append("windSpeed");
        sb.append('=');
        sb.append(((this.windSpeed == null)?"<null>":this.windSpeed));
        sb.append(',');
        sb.append("time");
        sb.append('=');
        sb.append(((this.time == null)?"<null>":this.time));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.pressure == null)? 0 :this.pressure.hashCode()));
        result = ((result* 31)+((this.tempDiff == null)? 0 :this.tempDiff.hashCode()));
        result = ((result* 31)+((this.rain24h == null)? 0 :this.rain24h.hashCode()));
        result = ((result* 31)+((this.rain12h == null)? 0 :this.rain12h.hashCode()));
        result = ((result* 31)+((this.rain6h == null)? 0 :this.rain6h.hashCode()));
        result = ((result* 31)+((this.temperature == null)? 0 :this.temperature.hashCode()));
        result = ((result* 31)+((this.humidity == null)? 0 :this.humidity.hashCode()));
        result = ((result* 31)+((this.rain1h == null)? 0 :this.rain1h.hashCode()));
        result = ((result* 31)+((this.windDirection == null)? 0 :this.windDirection.hashCode()));
        result = ((result* 31)+((this.time == null)? 0 :this.time.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.windSpeed == null)? 0 :this.windSpeed.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Passedchart) == false) {
            return false;
        }
        Passedchart rhs = ((Passedchart) other);
        return (((((((((((((this.pressure == rhs.pressure)||((this.pressure!= null)&&this.pressure.equals(rhs.pressure)))&&((this.tempDiff == rhs.tempDiff)||((this.tempDiff!= null)&&this.tempDiff.equals(rhs.tempDiff))))&&((this.rain24h == rhs.rain24h)||((this.rain24h!= null)&&this.rain24h.equals(rhs.rain24h))))&&((this.rain12h == rhs.rain12h)||((this.rain12h!= null)&&this.rain12h.equals(rhs.rain12h))))&&((this.rain6h == rhs.rain6h)||((this.rain6h!= null)&&this.rain6h.equals(rhs.rain6h))))&&((this.temperature == rhs.temperature)||((this.temperature!= null)&&this.temperature.equals(rhs.temperature))))&&((this.humidity == rhs.humidity)||((this.humidity!= null)&&this.humidity.equals(rhs.humidity))))&&((this.rain1h == rhs.rain1h)||((this.rain1h!= null)&&this.rain1h.equals(rhs.rain1h))))&&((this.windDirection == rhs.windDirection)||((this.windDirection!= null)&&this.windDirection.equals(rhs.windDirection))))&&((this.time == rhs.time)||((this.time!= null)&&this.time.equals(rhs.time))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.windSpeed == rhs.windSpeed)||((this.windSpeed!= null)&&this.windSpeed.equals(rhs.windSpeed))));
    }
}
