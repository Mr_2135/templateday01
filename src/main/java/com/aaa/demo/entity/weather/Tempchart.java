package com.aaa.demo.entity.weather;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "time",
        "max_temp",
        "min_temp",
        "day_img",
        "day_text",
        "night_img",
        "night_text"
})
public class Tempchart {
    @JsonProperty("time")
    private String time;
    @JsonProperty("max_temp")
    private Integer maxTemp;
    @JsonProperty("min_temp")
    private Double minTemp;
    @JsonProperty("day_img")
    private String dayImg;
    @JsonProperty("day_text")
    private String dayText;
    @JsonProperty("night_img")
    private String nightImg;
    @JsonProperty("night_text")
    private String nightText;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("time")
    public String getTime() {
        return time;
    }

    @JsonProperty("time")
    public void setTime(String time) {
        this.time = time;
    }

    @JsonProperty("max_temp")
    public Integer getMaxTemp() {
        return maxTemp;
    }

    @JsonProperty("max_temp")
    public void setMaxTemp(Integer maxTemp) {
        this.maxTemp = maxTemp;
    }

    @JsonProperty("min_temp")
    public Double getMinTemp() {
        return minTemp;
    }

    @JsonProperty("min_temp")
    public void setMinTemp(Double minTemp) {
        this.minTemp = minTemp;
    }

    @JsonProperty("day_img")
    public String getDayImg() {
        return dayImg;
    }

    @JsonProperty("day_img")
    public void setDayImg(String dayImg) {
        this.dayImg = dayImg;
    }

    @JsonProperty("day_text")
    public String getDayText() {
        return dayText;
    }

    @JsonProperty("day_text")
    public void setDayText(String dayText) {
        this.dayText = dayText;
    }

    @JsonProperty("night_img")
    public String getNightImg() {
        return nightImg;
    }

    @JsonProperty("night_img")
    public void setNightImg(String nightImg) {
        this.nightImg = nightImg;
    }

    @JsonProperty("night_text")
    public String getNightText() {
        return nightText;
    }

    @JsonProperty("night_text")
    public void setNightText(String nightText) {
        this.nightText = nightText;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Tempchart.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("time");
        sb.append('=');
        sb.append(((this.time == null)?"<null>":this.time));
        sb.append(',');
        sb.append("maxTemp");
        sb.append('=');
        sb.append(((this.maxTemp == null)?"<null>":this.maxTemp));
        sb.append(',');
        sb.append("minTemp");
        sb.append('=');
        sb.append(((this.minTemp == null)?"<null>":this.minTemp));
        sb.append(',');
        sb.append("dayImg");
        sb.append('=');
        sb.append(((this.dayImg == null)?"<null>":this.dayImg));
        sb.append(',');
        sb.append("dayText");
        sb.append('=');
        sb.append(((this.dayText == null)?"<null>":this.dayText));
        sb.append(',');
        sb.append("nightImg");
        sb.append('=');
        sb.append(((this.nightImg == null)?"<null>":this.nightImg));
        sb.append(',');
        sb.append("nightText");
        sb.append('=');
        sb.append(((this.nightText == null)?"<null>":this.nightText));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.nightText == null)? 0 :this.nightText.hashCode()));
        result = ((result* 31)+((this.dayImg == null)? 0 :this.dayImg.hashCode()));
        result = ((result* 31)+((this.maxTemp == null)? 0 :this.maxTemp.hashCode()));
        result = ((result* 31)+((this.nightImg == null)? 0 :this.nightImg.hashCode()));
        result = ((result* 31)+((this.time == null)? 0 :this.time.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.minTemp == null)? 0 :this.minTemp.hashCode()));
        result = ((result* 31)+((this.dayText == null)? 0 :this.dayText.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Tempchart) == false) {
            return false;
        }
        Tempchart rhs = ((Tempchart) other);
        return (((((((((this.nightText == rhs.nightText)||((this.nightText!= null)&&this.nightText.equals(rhs.nightText)))&&((this.dayImg == rhs.dayImg)||((this.dayImg!= null)&&this.dayImg.equals(rhs.dayImg))))&&((this.maxTemp == rhs.maxTemp)||((this.maxTemp!= null)&&this.maxTemp.equals(rhs.maxTemp))))&&((this.nightImg == rhs.nightImg)||((this.nightImg!= null)&&this.nightImg.equals(rhs.nightImg))))&&((this.time == rhs.time)||((this.time!= null)&&this.time.equals(rhs.time))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.minTemp == rhs.minTemp)||((this.minTemp!= null)&&this.minTemp.equals(rhs.minTemp))))&&((this.dayText == rhs.dayText)||((this.dayText!= null)&&this.dayText.equals(rhs.dayText))));
    }
}
