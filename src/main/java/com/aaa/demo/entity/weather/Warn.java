package com.aaa.demo.entity.weather;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "alert",
        "pic",
        "province",
        "city",
        "url",
        "issuecontent",
        "fmeans",
        "signaltype",
        "signallevel",
        "pic2"
})
public class Warn {
    @JsonProperty("alert")
    private String alert;
    @JsonProperty("pic")
    private String pic;
    @JsonProperty("province")
    private String province;
    @JsonProperty("city")
    private String city;
    @JsonProperty("url")
    private String url;
    @JsonProperty("issuecontent")
    private String issuecontent;
    @JsonProperty("fmeans")
    private String fmeans;
    @JsonProperty("signaltype")
    private String signaltype;
    @JsonProperty("signallevel")
    private String signallevel;
    @JsonProperty("pic2")
    private String pic2;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("alert")
    public String getAlert() {
        return alert;
    }

    @JsonProperty("alert")
    public void setAlert(String alert) {
        this.alert = alert;
    }

    @JsonProperty("pic")
    public String getPic() {
        return pic;
    }

    @JsonProperty("pic")
    public void setPic(String pic) {
        this.pic = pic;
    }

    @JsonProperty("province")
    public String getProvince() {
        return province;
    }

    @JsonProperty("province")
    public void setProvince(String province) {
        this.province = province;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty("issuecontent")
    public String getIssuecontent() {
        return issuecontent;
    }

    @JsonProperty("issuecontent")
    public void setIssuecontent(String issuecontent) {
        this.issuecontent = issuecontent;
    }

    @JsonProperty("fmeans")
    public String getFmeans() {
        return fmeans;
    }

    @JsonProperty("fmeans")
    public void setFmeans(String fmeans) {
        this.fmeans = fmeans;
    }

    @JsonProperty("signaltype")
    public String getSignaltype() {
        return signaltype;
    }

    @JsonProperty("signaltype")
    public void setSignaltype(String signaltype) {
        this.signaltype = signaltype;
    }

    @JsonProperty("signallevel")
    public String getSignallevel() {
        return signallevel;
    }

    @JsonProperty("signallevel")
    public void setSignallevel(String signallevel) {
        this.signallevel = signallevel;
    }

    @JsonProperty("pic2")
    public String getPic2() {
        return pic2;
    }

    @JsonProperty("pic2")
    public void setPic2(String pic2) {
        this.pic2 = pic2;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Warn.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("alert");
        sb.append('=');
        sb.append(((this.alert == null)?"<null>":this.alert));
        sb.append(',');
        sb.append("pic");
        sb.append('=');
        sb.append(((this.pic == null)?"<null>":this.pic));
        sb.append(',');
        sb.append("province");
        sb.append('=');
        sb.append(((this.province == null)?"<null>":this.province));
        sb.append(',');
        sb.append("city");
        sb.append('=');
        sb.append(((this.city == null)?"<null>":this.city));
        sb.append(',');
        sb.append("url");
        sb.append('=');
        sb.append(((this.url == null)?"<null>":this.url));
        sb.append(',');
        sb.append("issuecontent");
        sb.append('=');
        sb.append(((this.issuecontent == null)?"<null>":this.issuecontent));
        sb.append(',');
        sb.append("fmeans");
        sb.append('=');
        sb.append(((this.fmeans == null)?"<null>":this.fmeans));
        sb.append(',');
        sb.append("signaltype");
        sb.append('=');
        sb.append(((this.signaltype == null)?"<null>":this.signaltype));
        sb.append(',');
        sb.append("signallevel");
        sb.append('=');
        sb.append(((this.signallevel == null)?"<null>":this.signallevel));
        sb.append(',');
        sb.append("pic2");
        sb.append('=');
        sb.append(((this.pic2 == null)?"<null>":this.pic2));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.signallevel == null)? 0 :this.signallevel.hashCode()));
        result = ((result* 31)+((this.province == null)? 0 :this.province.hashCode()));
        result = ((result* 31)+((this.alert == null)? 0 :this.alert.hashCode()));
        result = ((result* 31)+((this.city == null)? 0 :this.city.hashCode()));
        result = ((result* 31)+((this.fmeans == null)? 0 :this.fmeans.hashCode()));
        result = ((result* 31)+((this.signaltype == null)? 0 :this.signaltype.hashCode()));
        result = ((result* 31)+((this.pic == null)? 0 :this.pic.hashCode()));
        result = ((result* 31)+((this.issuecontent == null)? 0 :this.issuecontent.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.pic2 == null)? 0 :this.pic2 .hashCode()));
        result = ((result* 31)+((this.url == null)? 0 :this.url.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Warn) == false) {
            return false;
        }
        Warn rhs = ((Warn) other);
        return ((((((((((((this.signallevel == rhs.signallevel)||((this.signallevel!= null)&&this.signallevel.equals(rhs.signallevel)))&&((this.province == rhs.province)||((this.province!= null)&&this.province.equals(rhs.province))))&&((this.alert == rhs.alert)||((this.alert!= null)&&this.alert.equals(rhs.alert))))&&((this.city == rhs.city)||((this.city!= null)&&this.city.equals(rhs.city))))&&((this.fmeans == rhs.fmeans)||((this.fmeans!= null)&&this.fmeans.equals(rhs.fmeans))))&&((this.signaltype == rhs.signaltype)||((this.signaltype!= null)&&this.signaltype.equals(rhs.signaltype))))&&((this.pic == rhs.pic)||((this.pic!= null)&&this.pic.equals(rhs.pic))))&&((this.issuecontent == rhs.issuecontent)||((this.issuecontent!= null)&&this.issuecontent.equals(rhs.issuecontent))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.pic2 == rhs.pic2)||((this.pic2 != null)&&this.pic2 .equals(rhs.pic2))))&&((this.url == rhs.url)||((this.url!= null)&&this.url.equals(rhs.url))));
    }

}
