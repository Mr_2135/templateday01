package com.aaa.demo.entity.weather;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "temperature",
        "temperatureDiff",
        "airpressure",
        "humidity",
        "rain",
        "rcomfort",
        "icomfort",
        "info",
        "img",
        "feelst"
})
public class Weather {
    @JsonProperty("temperature")
    private Double temperature;
    @JsonProperty("temperatureDiff")
    private Double temperatureDiff;
    @JsonProperty("airpressure")
    private Integer airpressure;
    @JsonProperty("humidity")
    private Integer humidity;
    @JsonProperty("rain")
    private Integer rain;
    @JsonProperty("rcomfort")
    private Integer rcomfort;
    @JsonProperty("icomfort")
    private Integer icomfort;
    @JsonProperty("info")
    private String info;
    @JsonProperty("img")
    private String img;
    @JsonProperty("feelst")
    private Double feelst;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("temperature")
    public Double getTemperature() {
        return temperature;
    }

    @JsonProperty("temperature")
    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    @JsonProperty("temperatureDiff")
    public Double getTemperatureDiff() {
        return temperatureDiff;
    }

    @JsonProperty("temperatureDiff")
    public void setTemperatureDiff(Double temperatureDiff) {
        this.temperatureDiff = temperatureDiff;
    }

    @JsonProperty("airpressure")
    public Integer getAirpressure() {
        return airpressure;
    }

    @JsonProperty("airpressure")
    public void setAirpressure(Integer airpressure) {
        this.airpressure = airpressure;
    }

    @JsonProperty("humidity")
    public Integer getHumidity() {
        return humidity;
    }

    @JsonProperty("humidity")
    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }

    @JsonProperty("rain")
    public Integer getRain() {
        return rain;
    }

    @JsonProperty("rain")
    public void setRain(Integer rain) {
        this.rain = rain;
    }

    @JsonProperty("rcomfort")
    public Integer getRcomfort() {
        return rcomfort;
    }

    @JsonProperty("rcomfort")
    public void setRcomfort(Integer rcomfort) {
        this.rcomfort = rcomfort;
    }

    @JsonProperty("icomfort")
    public Integer getIcomfort() {
        return icomfort;
    }

    @JsonProperty("icomfort")
    public void setIcomfort(Integer icomfort) {
        this.icomfort = icomfort;
    }

    @JsonProperty("info")
    public String getInfo() {
        return info;
    }

    @JsonProperty("info")
    public void setInfo(String info) {
        this.info = info;
    }

    @JsonProperty("img")
    public String getImg() {
        return img;
    }

    @JsonProperty("img")
    public void setImg(String img) {
        this.img = img;
    }

    @JsonProperty("feelst")
    public Double getFeelst() {
        return feelst;
    }

    @JsonProperty("feelst")
    public void setFeelst(Double feelst) {
        this.feelst = feelst;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Weather.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("temperature");
        sb.append('=');
        sb.append(((this.temperature == null)?"<null>":this.temperature));
        sb.append(',');
        sb.append("temperatureDiff");
        sb.append('=');
        sb.append(((this.temperatureDiff == null)?"<null>":this.temperatureDiff));
        sb.append(',');
        sb.append("airpressure");
        sb.append('=');
        sb.append(((this.airpressure == null)?"<null>":this.airpressure));
        sb.append(',');
        sb.append("humidity");
        sb.append('=');
        sb.append(((this.humidity == null)?"<null>":this.humidity));
        sb.append(',');
        sb.append("rain");
        sb.append('=');
        sb.append(((this.rain == null)?"<null>":this.rain));
        sb.append(',');
        sb.append("rcomfort");
        sb.append('=');
        sb.append(((this.rcomfort == null)?"<null>":this.rcomfort));
        sb.append(',');
        sb.append("icomfort");
        sb.append('=');
        sb.append(((this.icomfort == null)?"<null>":this.icomfort));
        sb.append(',');
        sb.append("info");
        sb.append('=');
        sb.append(((this.info == null)?"<null>":this.info));
        sb.append(',');
        sb.append("img");
        sb.append('=');
        sb.append(((this.img == null)?"<null>":this.img));
        sb.append(',');
        sb.append("feelst");
        sb.append('=');
        sb.append(((this.feelst == null)?"<null>":this.feelst));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.airpressure == null)? 0 :this.airpressure.hashCode()));
        result = ((result* 31)+((this.rain == null)? 0 :this.rain.hashCode()));
        result = ((result* 31)+((this.rcomfort == null)? 0 :this.rcomfort.hashCode()));
        result = ((result* 31)+((this.img == null)? 0 :this.img.hashCode()));
        result = ((result* 31)+((this.temperature == null)? 0 :this.temperature.hashCode()));
        result = ((result* 31)+((this.humidity == null)? 0 :this.humidity.hashCode()));
        result = ((result* 31)+((this.feelst == null)? 0 :this.feelst.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.temperatureDiff == null)? 0 :this.temperatureDiff.hashCode()));
        result = ((result* 31)+((this.icomfort == null)? 0 :this.icomfort.hashCode()));
        result = ((result* 31)+((this.info == null)? 0 :this.info.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Weather) == false) {
            return false;
        }
        Weather rhs = ((Weather) other);
        return ((((((((((((this.airpressure == rhs.airpressure)||((this.airpressure!= null)&&this.airpressure.equals(rhs.airpressure)))&&((this.rain == rhs.rain)||((this.rain!= null)&&this.rain.equals(rhs.rain))))&&((this.rcomfort == rhs.rcomfort)||((this.rcomfort!= null)&&this.rcomfort.equals(rhs.rcomfort))))&&((this.img == rhs.img)||((this.img!= null)&&this.img.equals(rhs.img))))&&((this.temperature == rhs.temperature)||((this.temperature!= null)&&this.temperature.equals(rhs.temperature))))&&((this.humidity == rhs.humidity)||((this.humidity!= null)&&this.humidity.equals(rhs.humidity))))&&((this.feelst == rhs.feelst)||((this.feelst!= null)&&this.feelst.equals(rhs.feelst))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.temperatureDiff == rhs.temperatureDiff)||((this.temperatureDiff!= null)&&this.temperatureDiff.equals(rhs.temperatureDiff))))&&((this.icomfort == rhs.icomfort)||((this.icomfort!= null)&&this.icomfort.equals(rhs.icomfort))))&&((this.info == rhs.info)||((this.info!= null)&&this.info.equals(rhs.info))));
    }

}
