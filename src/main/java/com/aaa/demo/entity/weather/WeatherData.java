package com.aaa.demo.entity.weather;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @description:
 * @author: Administrator
 * @date: 2022-06-22 11:04
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "real",
        "predict",
        "air",
        "tempchart",
        "passedchart",
        "climate",
        "radar"
})
public class WeatherData {
    @JsonProperty("real")
    private Real real;
    @JsonProperty("predict")
    private Predict predict;
    @JsonProperty("air")
    private Air air;
    @JsonProperty("tempchart")
    private List<Tempchart> tempchart = new ArrayList<Tempchart>();
    @JsonProperty("passedchart")
    private List<Passedchart> passedchart = new ArrayList<Passedchart>();
    @JsonProperty("climate")
    private Climate climate;
    @JsonProperty("radar")
    private Radar radar;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("real")
    public Real getReal() {
        return real;
    }

    @JsonProperty("real")
    public void setReal(Real real) {
        this.real = real;
    }

    @JsonProperty("predict")
    public Predict getPredict() {
        return predict;
    }

    @JsonProperty("predict")
    public void setPredict(Predict predict) {
        this.predict = predict;
    }

    @JsonProperty("air")
    public Air getAir() {
        return air;
    }

    @JsonProperty("air")
    public void setAir(Air air) {
        this.air = air;
    }

    @JsonProperty("tempchart")
    public List<Tempchart> getTempchart() {
        return tempchart;
    }

    @JsonProperty("tempchart")
    public void setTempchart(List<Tempchart> tempchart) {
        this.tempchart = tempchart;
    }

    @JsonProperty("passedchart")
    public List<Passedchart> getPassedchart() {
        return passedchart;
    }

    @JsonProperty("passedchart")
    public void setPassedchart(List<Passedchart> passedchart) {
        this.passedchart = passedchart;
    }

    @JsonProperty("climate")
    public Climate getClimate() {
        return climate;
    }

    @JsonProperty("climate")
    public void setClimate(Climate climate) {
        this.climate = climate;
    }

    @JsonProperty("radar")
    public Radar getRadar() {
        return radar;
    }

    @JsonProperty("radar")
    public void setRadar(Radar radar) {
        this.radar = radar;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(WeatherData.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("real");
        sb.append('=');
        sb.append(((this.real == null)?"<null>":this.real));
        sb.append(',');
        sb.append("predict");
        sb.append('=');
        sb.append(((this.predict == null)?"<null>":this.predict));
        sb.append(',');
        sb.append("air");
        sb.append('=');
        sb.append(((this.air == null)?"<null>":this.air));
        sb.append(',');
        sb.append("tempchart");
        sb.append('=');
        sb.append(((this.tempchart == null)?"<null>":this.tempchart));
        sb.append(',');
        sb.append("passedchart");
        sb.append('=');
        sb.append(((this.passedchart == null)?"<null>":this.passedchart));
        sb.append(',');
        sb.append("climate");
        sb.append('=');
        sb.append(((this.climate == null)?"<null>":this.climate));
        sb.append(',');
        sb.append("radar");
        sb.append('=');
        sb.append(((this.radar == null)?"<null>":this.radar));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.radar == null)? 0 :this.radar.hashCode()));
        result = ((result* 31)+((this.tempchart == null)? 0 :this.tempchart.hashCode()));
        result = ((result* 31)+((this.predict == null)? 0 :this.predict.hashCode()));
        result = ((result* 31)+((this.passedchart == null)? 0 :this.passedchart.hashCode()));
        result = ((result* 31)+((this.real == null)? 0 :this.real.hashCode()));
        result = ((result* 31)+((this.air == null)? 0 :this.air.hashCode()));
        result = ((result* 31)+((this.climate == null)? 0 :this.climate.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof WeatherData) == false) {
            return false;
        }
        WeatherData rhs = ((WeatherData) other);
        return (((((((((this.radar == rhs.radar)||((this.radar!= null)&&this.radar.equals(rhs.radar)))&&((this.tempchart == rhs.tempchart)||((this.tempchart!= null)&&this.tempchart.equals(rhs.tempchart))))&&((this.predict == rhs.predict)||((this.predict!= null)&&this.predict.equals(rhs.predict))))&&((this.passedchart == rhs.passedchart)||((this.passedchart!= null)&&this.passedchart.equals(rhs.passedchart))))&&((this.real == rhs.real)||((this.real!= null)&&this.real.equals(rhs.real))))&&((this.air == rhs.air)||((this.air!= null)&&this.air.equals(rhs.air))))&&((this.climate == rhs.climate)||((this.climate!= null)&&this.climate.equals(rhs.climate))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))));
    }
}
