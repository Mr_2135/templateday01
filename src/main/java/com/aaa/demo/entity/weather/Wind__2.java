package com.aaa.demo.entity.weather;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "direct",
        "power"
})
public class Wind__2 {
    @JsonProperty("direct")
    private String direct;
    @JsonProperty("power")
    private String power;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("direct")
    public String getDirect() {
        return direct;
    }

    @JsonProperty("direct")
    public void setDirect(String direct) {
        this.direct = direct;
    }

    @JsonProperty("power")
    public String getPower() {
        return power;
    }

    @JsonProperty("power")
    public void setPower(String power) {
        this.power = power;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Wind__2 .class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("direct");
        sb.append('=');
        sb.append(((this.direct == null)?"<null>":this.direct));
        sb.append(',');
        sb.append("power");
        sb.append('=');
        sb.append(((this.power == null)?"<null>":this.power));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.direct == null)? 0 :this.direct.hashCode()));
        result = ((result* 31)+((this.power == null)? 0 :this.power.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Wind__2) == false) {
            return false;
        }
        Wind__2 rhs = ((Wind__2) other);
        return ((((this.direct == rhs.direct)||((this.direct!= null)&&this.direct.equals(rhs.direct)))&&((this.power == rhs.power)||((this.power!= null)&&this.power.equals(rhs.power))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))));
    }
}
