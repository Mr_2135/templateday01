package com.aaa.demo.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @description:
 * @author: Administrator
 * @date: 2021-12-20 10:21
 */
public enum GradeEnum {
    PRIMARY(1, "小学"),  SECONDORY(2, "中学"),  HIGH(3, "高中");

    GradeEnum(int code, String descp) {
        this.code = code;
        this.descp = descp;
    }

    @EnumValue
    @JsonValue    //标记响应json值
    private final int code;
    @EnumValue
    private final String descp;
}
