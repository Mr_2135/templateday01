package com.aaa.demo.enums;

import java.util.Arrays;

/**
 * @description:
 * @author: Administrator
 * @date: 2021-08-19 11:23
 */
public enum  ImgType {
    IMAGE_TYPE_GIF("gif"),
    IMAGE_TYPE_JPG("jpg"),
    IMAGE_TYPE_JPEG("jpeg"),
    IMAGE_TYPE_BMP("bmp"),
    IMAGE_TYPE_PNG("png"),
    IMAGE_TYPE_PSD("psd");

    ImgType(String values) {
        this.values = values;
    }

    private String values;

    public String getValues() {
        return values;
    }

    public static boolean getImgType(String type){
        ImgType[] values = ImgType.values();
        return Arrays.stream(values).anyMatch(getTyp -> getTyp.getValues().equals(type));
    }
}
