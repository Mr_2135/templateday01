package com.aaa.demo.enums;

/**
 * 用户状态
 * 
 * @author Mr.2135
 */
public enum UserStatus
{
    OK("0", "正常"),
    DISABLE("1", "停用"),
    DELETED("2", "删除"),
    DOESNOTEXIST("3","用户不存在");

    private final String code;
    private final String info;

    UserStatus(String code, String info)
    {
        this.code = code;
        this.info = info;
    }

    public String getCode()
    {
        return code;
    }

    public String getInfo()
    {
        return info;
    }
}
