package com.aaa.demo.exception;

import cn.hutool.http.HttpStatus;
import com.aaa.demo.entity.resultmodel.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;


import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.exception.NotPermissionException;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * @description: 全局异常拦截
 * @author: Mr.2135
 * @date: 2021-10-15
 */
@RestControllerAdvice
@Slf4j
public class GlobalException {

    @ExceptionHandler(NotLoginException.class)
    public JsonResult handlerNotLoginException(NotLoginException e) {
        String type = e.getType();
        log.info("异常的场景值：{}", type);
        String mes = "";
        switch (type) {
            case NotLoginException.NOT_TOKEN:
                mes = "未登录,请先登录";
                break;
            case NotLoginException.INVALID_TOKEN:
                mes = "请重新登录";
                break;
            case NotLoginException.TOKEN_TIMEOUT:
                mes = "长时间未操作，请重新登录";
                break;
            case NotLoginException.BE_REPLACED:
                mes = "账户在别处登录，不是本人。及时修改密码";
                break;
            case NotLoginException.KICK_OUT:
                mes = "你被管理员踢下线";
                break;
            default:
                mes = "未登录";
                break;

        }
        return JsonResult.errorMsg(mes);
    }
    @ExceptionHandler(NotPermissionException.class)
    public JsonResult handlerNotPermissionException(NotPermissionException e, HttpServletRequest request) {
        String requestUrl = request.getRequestURI();
        log.error("请求地址'{}',发生无权限异常'{}'", requestUrl, e);
        return JsonResult.errorMsgAndCode(HttpStatus.HTTP_FORBIDDEN, "没有权限进行访问");
    }

}
