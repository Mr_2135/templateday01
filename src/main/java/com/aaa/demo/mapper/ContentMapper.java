package com.aaa.demo.mapper;

import com.aaa.demo.entity.Content;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 信息发布 Mapper 接口
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
public interface ContentMapper extends BaseMapper<Content> {

}
