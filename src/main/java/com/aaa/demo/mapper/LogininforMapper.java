package com.aaa.demo.mapper;

import com.aaa.demo.entity.Logininfor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 系统访问记录 Mapper 接口
 * </p>
 *
 * @author Mr.2135
 * @since 2021-12-27
 */
@Mapper
@Repository
public interface LogininforMapper extends BaseMapper<Logininfor> {
    /**
     * 查询登录日志列表
     * @param loginName 登录名称
     * @param loginLocation 登录地址
     * @param status 状态
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param pageNum 页码
     * @param pageSize 多少条
     * @return 返回
     */
    List<Logininfor> selectLogininforList(String loginName, String loginLocation, String status,
                                          String beginTime, String endTime, Integer pageNum, Integer pageSize);

    /**
     * 统计条件查询到多少条
     * @param loginName 登录名称
     * @param loginLocation 登录地址
     * @param status 状态
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @return 返回查询到多少条
     */
    Integer selectLogininforCount(String loginName, String loginLocation, String status,
                                  String beginTime, String endTime);

    /**
     * 登录日志新增
     * @param logininfor 登录日志信息
     * @return 是否成功
     */
    Integer insertLogininfor(Logininfor logininfor);

    /**
     * 批量删除用户登录日志
     *
     * @param infoIds 需要删除的用户登录日志ID
     * @return 结果
     */
    int deleteLogininforByIds(Long[] infoIds);

    /**
     * 清空登录日志
     * @return 是否成功
     */
    void cleanLogininfor();
}
