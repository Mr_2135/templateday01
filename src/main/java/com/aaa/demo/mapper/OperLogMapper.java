package com.aaa.demo.mapper;


import com.aaa.demo.entity.OperLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 操作日志记录 Mapper 接口
 * </p>
 *
 * @author Mr.2135
 * @since 2022-02-09
 */
@Mapper
@Repository
public interface OperLogMapper extends BaseMapper<OperLog> {

    /**
     * 新增操作日志
     *
     * @param operLog 操作日志对象
     */
    public void insertOperlog(OperLog operLog);

    /**
     * 查询系统操作日志集合
     *
     * @return 操作日志集合
     */
    public List<OperLog> selectOperLogList(String title, Integer businessType, String status,
                                           String operName, String beginTime, String endTime,
                                           Integer pageNum, Integer pageSize);

    public Integer selectOperLogCount(String title, Integer businessType, String status,
                                      String operName, String beginTime, String endTime);
    /**
     * 批量删除系统操作日志
     *
     * @param operIds 需要删除的操作日志ID
     * @return 结果
     */
    public int deleteOperLogByIds(Long[] operIds);

    /**
     * 查询操作日志详细
     *
     * @param operId 操作ID
     * @return 操作日志对象
     */
    public OperLog selectOperLogById(Long operId);

    /**
     * 清空操作日志
     */
    public void cleanOperLog();
}
