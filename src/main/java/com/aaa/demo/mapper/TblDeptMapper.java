package com.aaa.demo.mapper;

import com.aaa.demo.entity.TblDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
@Repository
@Mapper
public interface TblDeptMapper extends BaseMapper<TblDept> {
    List<TblDept> deptList(String deptName);
    int deptConut(String deptName);
    int deptConut(String deptName,Integer deptId);
    int deptAdd(TblDept dept);
    int deptUpdate(TblDept dept);
    int deptCheckDuplicate(String deptId,String deptName);
    int deptDelete(TblDept dept);
    Set<String> selectDeptByList(List<Integer> list);
}
