package com.aaa.demo.mapper;

import com.aaa.demo.entity.TblMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 菜单权限表 Mapper 接口
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
@Mapper
@Repository
public interface TblMenuMapper extends BaseMapper<TblMenu> {

    List<String> selectMenuPermsByUserId(Integer userId);

    List<TblMenu> selectMenuTreeAll();

    List<TblMenu> menuTreeList(Integer userId);
}
