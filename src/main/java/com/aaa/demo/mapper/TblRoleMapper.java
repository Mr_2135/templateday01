package com.aaa.demo.mapper;

import com.aaa.demo.entity.TblRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 角色信息表 Mapper 接口
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
@Mapper
@Repository
public interface TblRoleMapper extends BaseMapper<TblRole> {
    List<TblRole> roleList(Integer pageNum,Integer PageSize,String roleByName);

    int roleCount(String roleByName);

    int roleAdd(TblRole role);

    int updateRole(TblRole role);
}
