package com.aaa.demo.mapper;

import com.aaa.demo.entity.TblRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 角色和菜单关联表 Mapper 接口
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
@Mapper
@Repository
public interface TblRoleMenuMapper extends BaseMapper<TblRoleMenu> {
    int addRoleMenu(Integer roleId,Integer[] menuId);

    int deleteRoleMenu(Integer roleId);
}
