package com.aaa.demo.mapper;

import com.aaa.demo.entity.TblMenu;
import com.aaa.demo.entity.TblUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.omg.PortableInterceptor.INACTIVE;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
@Mapper
@Repository
public interface TblUserMapper extends BaseMapper<TblUser> {
    List<TblMenu> selectMenuByUserId(Integer userId);
    TblUser selectByUserName(String loginName);
    TblUser selectByUserId(Integer userId);
    List<TblUser> userList(Integer pageNum,Integer pageSize,String userName);
    Map<String, Object> getUserInfo(Integer userId);
    int userListCount(String userName);

}
