package com.aaa.demo.mapper;

import com.aaa.demo.entity.TblUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户和角色关联表 Mapper 接口
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
public interface TblUserRoleMapper extends BaseMapper<TblUserRole> {

}
