package com.aaa.demo.mapper;

import com.aaa.demo.entity.Userinfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
public interface UserinfoMapper extends BaseMapper<Userinfo> {

}
