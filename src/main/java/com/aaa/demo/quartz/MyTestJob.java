package com.aaa.demo.quartz;

import cn.hutool.core.date.DateUtil;
import com.aaa.demo.quartz.base.BaseJob;
import com.aaa.demo.quartz.form.JobForm;
import com.aaa.demo.quartz.service.JobService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @description: 定时任务业务逻辑
 * @author: Mr.2135
 * @date: 2022-06-24 10:06
 */
@Slf4j
public class MyTestJob  implements BaseJob {
    @Autowired
    private JobService jobService;

    @SneakyThrows
    @Override
    public void execute(JobExecutionContext context){
        log.info("Test Job 执行时间: {}", DateUtil.now());
        JobForm form = new JobForm();
        form.setJobGroupName("MyTestJob");
        form.setJobClassName("com.aaa.demo.quartz.task.MyTestJob");
        jobService.deleteJob(form);
    }
}
