package com.aaa.demo.quartz.controller;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.unit.DataUnit;
import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.aaa.demo.core.controller.BaseController;
import com.aaa.demo.entity.TblDept;
import com.aaa.demo.entity.page.TableDataInfo;
import com.aaa.demo.entity.resultmodel.AjaxResult;
import com.aaa.demo.quartz.domain.JobAndTrigger;
import com.aaa.demo.quartz.form.JobForm;
import com.aaa.demo.quartz.service.JobService;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.helper.DataUtil;
import org.quartz.Job;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * Job Controller
 * </p>
 *
 * @author yangkai.shen
 * @date Created in 2018-11-26 13:23
 */
@RestController
@RequestMapping("/job")
@Slf4j
public class JobController extends BaseController {
    private final JobService jobService;

    @Autowired
    public JobController(JobService jobService) {
        this.jobService = jobService;
    }

    /**
     * 保存定时任务
     */
    @PostMapping
    public AjaxResult addJob(@RequestBody @Valid JobForm form) {
        try {
            jobService.addJob(form);
        } catch (Exception e) {
            return AjaxResult.error();
        }

        return AjaxResult.success();
    }

    /**
     * 根据日期生成cron 表达式
     */
    @PostMapping("/addJobByBate")
    public AjaxResult addJobByBate(JobForm form) {
        try {
            jobService.addJobByBate(form);
        } catch (Exception e) {
            return AjaxResult.error();
        }
        return AjaxResult.success();
    }

    /**
     * 删除定时任务
     */
    @DeleteMapping("/deleteJob")
    public AjaxResult deleteJob(@RequestBody JobForm form) throws SchedulerException {
        if (StrUtil.hasBlank(form.getJobGroupName(), form.getJobClassName())) {
            return AjaxResult.error("参数不能为空");
        }
        jobService.deleteJob(form);
        return AjaxResult.success();
    }

    /**
     * 暂停定时任务
     */
    @PutMapping(params = "pause")
    public AjaxResult pauseJob(@RequestBody JobForm form) throws SchedulerException {
        if (StrUtil.hasBlank(form.getJobGroupName(), form.getJobClassName())) {
            return AjaxResult.error("参数不能为空");
        }
        jobService.pauseJob(form);
        return AjaxResult.success();
    }

    /**
     * 恢复定时任务
     */
    @PutMapping(params = "resume")
    public AjaxResult resumeJob(@RequestBody JobForm form) throws SchedulerException {
        if (StrUtil.hasBlank(form.getJobGroupName(), form.getJobClassName())) {
            return AjaxResult.error("参数不能为空");
        }

        jobService.resumeJob(form);
        return AjaxResult.success("恢复成功");
    }

    /**
     * 修改定时任务，定时时间
     */
    @PutMapping(params = "cron")
    public AjaxResult cronJob(@RequestBody @Valid JobForm form) {
        try {
            jobService.cronJob(form);
        } catch (Exception e) {
            return AjaxResult.error("修改失败");
        }

        return AjaxResult.success();
    }

    @GetMapping
    public TableDataInfo jobList(Integer currentPage, Integer pageSize) {
        startPage();
        List<JobAndTrigger> all = jobService.list(currentPage, pageSize);
        return getDataTable(all);
    }

}
