package com.aaa.demo.quartz.task;

import com.aaa.demo.quartz.base.BaseJob;
import com.aaa.demo.quartz.form.JobForm;
import com.aaa.demo.quartz.service.JobService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @description: 定时任务调度测试
 * @author: Mr.2135
 * @date: 2022-07-19 17:10
 */
@Slf4j
@Component("ryTask")
public class RyTask implements BaseJob {

    @Autowired
    private JobService jobService;

    @SneakyThrows
    @Override
    public void execute(JobExecutionContext context){
        JobDataMap map = context.getMergedJobDataMap();
        String nextIdStr = null;
        for (String str : map.keySet()) {
            nextIdStr = map.getString(str);
        }
        demo(nextIdStr);
//        JobForm form = new JobForm();
//        form.setJobGroupName("有参指令");
//        form.setJobClassName("com.aaa.demo.quartz.RyTask");
//        jobService.deleteJob(form);
    }

    public void demo(String nextId){
        log.info("获取到参数：{}",nextId);
    }
}
