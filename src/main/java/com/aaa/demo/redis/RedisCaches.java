package com.aaa.demo.redis;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.cache.Cache;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.DigestUtils;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 自定义redis缓存
 */
@Slf4j
public class RedisCaches implements Cache {

    //当前放入缓存的mapper的namaspace
    private final String id;
    /**
     *    redis过期时间
     */
    private static final long EXPIRE_TIME_IN_MINUTES = 2;
    /**
     * 读写锁
     */
    private static ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    //必须存在构造方法
    public RedisCaches(String id) {
        if (id == null) {
            throw new IllegalArgumentException("Cache instances require an ID");
        }
        this.id = id;
    }

    @Override
    //返回值为空就会报（name argument cannot be null） 返回Cache的唯一标识
    public String getId() {
        return this.id;
    }

    //缓存放入值
    @Override
    public void putObject(Object key, Object value) {
        //Application工具类获取redisTemplate
//        log.info("开始储存");
        getRedisTemplate().opsForHash().put (id.toString(), getMd5(key.toString()), value);
        getRedisTemplate().expire(id.toString(),EXPIRE_TIME_IN_MINUTES, TimeUnit.DAYS);
    }

    //获取中 获取数据
    @Override
    public Object getObject(Object key) {
//        log.info("获取值");
        return getRedisTemplate().opsForHash().get(id.toString(), getMd5(key.toString()));
    }

    /**
     * 根据key 删除缓存
     * 这个方法为mybatis 的保留方法，默认没有实现
     * @param key
     * @return
     */
    @Override
    public Object removeObject(Object key) {
        return null;
    }

    /**
     * 清空缓存
     * 只要执行 增删改 就会清空缓存
     */
    @Override
    public void clear() {
//        log.info("清空缓存");
        getRedisTemplate().delete(id.toString());
    }

    /**
     * 获取缓存的长度
     * @return
     */
    @Override
    public int getSize() {
        return 0;
    }

    /**
     * 读写锁
     *
     * @return
     */
    @Override
    public ReadWriteLock getReadWriteLock() {
        return readWriteLock;
    }
    //封装redisTemplate
    private RedisTemplate getRedisTemplate() {
        RedisTemplate redisTemplate =  ApplicationConextUtils.getBean("redisTemplate");
        return redisTemplate;
    }
    // 封装一个对key进行Md5处理方法

    /**
     * 对放入redis中key进行优化；key的长度不能太长
     * @param key
     * @return
     */
    private String getMd5(String key) {
        return DigestUtils.md5DigestAsHex(key.getBytes());
    }
}
