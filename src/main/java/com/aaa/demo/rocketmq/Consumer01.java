//package com.aaa.demo.rocketmq;
//
//import lombok.extern.slf4j.Slf4j;
//import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
//import org.apache.rocketmq.spring.core.RocketMQListener;
//import org.springframework.stereotype.Component;
//
///**
// * @description: 消费端
// * @author: Mr.2135
// * @date: 2021-10-26 14:30
// */
//@Component
//@RocketMQMessageListener(consumerGroup = "my_producer_group", topic = "first-topic01")
//@Slf4j
//public class Consumer01 implements RocketMQListener<String> {
//
//    @Override
//    public void onMessage(String message)
//    {
//        log.info("我收到的消息！消息内容为：{}",message);
//    }
//}
