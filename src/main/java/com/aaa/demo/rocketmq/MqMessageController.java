//package com.aaa.demo.rocketmq;
//
//import lombok.extern.slf4j.Slf4j;
//import org.apache.rocketmq.client.producer.DefaultMQProducer;
//import org.apache.rocketmq.spring.core.RocketMQTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
///**
// * @description: mq测试
// * @author: Mr.2135
// * @date: 2021-10-25 16:56
// */
//
//@Slf4j
//@RestController
//@RequestMapping("mqMessageController")
//public class MqMessageController {
//
//    private RocketMQTemplate rocketmqtemplate;
//
//    @Autowired
//    public MqMessageController(RocketMQTemplate rocketmqtemplate){
//        this.rocketmqtemplate = rocketmqtemplate;
//    }
//
//    /**
//     * 生产端
//     * @param name
//     * @return
//     */
//    @PostMapping("/getMqMessage")
//    public String getMqMessage(String name,String message){
//        DefaultMQProducer producer = new DefaultMQProducer("my_producer_group");
//        producer.setCreateTopicKey("AUTO_CREATE_TOPIC_KEY");
//        if (!"".equals(message)){
//            rocketmqtemplate.convertAndSend(name,message);
//        }
//        log.info("获取到ID：{}",name);
//        return "成功";
//    }
//
//}
