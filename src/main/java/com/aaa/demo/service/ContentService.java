package com.aaa.demo.service;

import com.aaa.demo.entity.Content;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 信息发布 服务类
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
public interface ContentService extends IService<Content> {

}
