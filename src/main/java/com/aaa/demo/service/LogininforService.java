package com.aaa.demo.service;


import com.aaa.demo.entity.Logininfor;
import com.aaa.demo.entity.resultmodel.JsonResult;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统访问记录 服务类
 * </p>
 *
 * @author Mr.2135
 * @since 2021-12-27
 */
public interface LogininforService extends IService<Logininfor> {
    /**
     * 登录记录日志
     * @param logininfor
     * @return 结果
     */
    void recordLogininfor(Logininfor logininfor);
    /**
     * 查询登录日志列表
     * @param loginName 登录名称
     * @param loginLocation 登录地址
     * @param status 状态
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param pageNum 页码
     * @param pageSize 多少条
     * @return 返回
     */
    JsonResult selectLogininforList(String loginName, String loginLocation, String status,
                                    String beginTime, String endTime, Integer pageNum, Integer pageSize);

    /**
     * 批量删除用户登录日志
     *
     * @param infoIds 需要删除的用户登录日志ID
     * @return 结果
     */
    JsonResult deleteLogininforByIds(Long[] infoIds);

    /**
     * 清空登录日志
     * @return 是否成功
     */
    JsonResult cleanLogininfor();
}
