package com.aaa.demo.service;


import com.aaa.demo.entity.OperLog;
import com.aaa.demo.entity.resultmodel.JsonResult;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 操作日志记录 服务类
 * </p>
 *
 * @author Mr.2135
 * @since 2022-02-09
 */
public interface OperLogService extends IService<OperLog> {

    /**
     * 新增操作日志
     *
     * @param operLog 操作日志对象
     */
    public void insertOperlog(OperLog operLog);

    /**
     * 查询系统操作日志集合
     * @param title 模块标题
     * @param businessType 业务类型
     * @param status 状态
     * @param operName 操作人员
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param pageNum 当前页
     * @param pageSize 多少条
     * @return 返回
     */
    public JsonResult selectOperLogList(String title, Integer businessType, String status,
                                        String operName, String beginTime, String endTime,
                                        Integer pageNum, Integer pageSize);

    /**
     * 批量删除系统操作日志
     *
     * @param operIds 需要删除的操作日志ID
     * @return 结果
     */
    public JsonResult deleteOperLogByIds(Long[] operIds);

    /**
     * 查询操作日志详细
     *
     * @param operId 操作ID
     * @return 操作日志对象
     */
    public JsonResult selectOperLogById(Long operId);

    /**
     * 清空操作日志
     */
    public void cleanOperLog();

}
