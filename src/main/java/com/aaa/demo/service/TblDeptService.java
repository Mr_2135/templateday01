package com.aaa.demo.service;

import com.aaa.demo.entity.TblDept;
import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.demo.entity.resultmodel.JsonResult;

import java.util.List;

/**
 * <p>
 * 部门表 服务类
 * </p>
 *
 * @author cwp
 */

public interface TblDeptService extends IService<TblDept> {
    List<TblDept> deptList( String deptName);

    JsonResult deptAdd(TblDept dept);
    JsonResult deptUpdate(TblDept dept);
    JsonResult deptDelete(TblDept dept);
}
