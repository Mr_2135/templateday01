package com.aaa.demo.service;

import com.aaa.demo.entity.TblMenu;
import com.aaa.demo.entity.TblUser;
import com.aaa.demo.entity.resultmodel.AjaxResult;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Set;

/**
 * <p>
 * 菜单权限表 服务类
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
public interface TblMenuService extends IService<TblMenu> {
    /**
     * 获取用户拥有的权限
     * @param admin 信息
     * @return 结果
     */
    Set<String> selectMenuByUserId(TblUser admin);

    AjaxResult getRouters(Integer userId);

}
