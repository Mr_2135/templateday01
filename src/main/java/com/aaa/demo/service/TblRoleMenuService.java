package com.aaa.demo.service;

import com.aaa.demo.entity.TblRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色和菜单关联表 服务类
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
public interface TblRoleMenuService extends IService<TblRoleMenu> {

}
