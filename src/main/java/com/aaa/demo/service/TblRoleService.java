package com.aaa.demo.service;

import com.aaa.demo.entity.TblRole;
import com.baomidou.mybatisplus.extension.service.IService;
import com.aaa.demo.entity.resultmodel.JsonResult;

/**
 * <p>
 * 角色信息表 服务类
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
public interface TblRoleService extends IService<TblRole> {
    JsonResult roleList(Integer pageNum, Integer pageSize, String roleByName);


    JsonResult roleAdd(TblRole role, Integer[] roleId);

    JsonResult updateRole(TblRole role);
}
