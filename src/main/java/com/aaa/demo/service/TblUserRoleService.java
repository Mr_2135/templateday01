package com.aaa.demo.service;

import com.aaa.demo.entity.TblUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户和角色关联表 服务类
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
public interface TblUserRoleService extends IService<TblUserRole> {

}
