package com.aaa.demo.service;

import com.aaa.demo.entity.resultmodel.AjaxResult;
import com.aaa.demo.entity.resultmodel.JsonResult;
import com.aaa.demo.entity.TblUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
public interface TblUserService extends IService<TblUser> {

    AjaxResult login(String username, String password, String code, String uui);

    /**
     * 根据用户编号获取拥有的权限
     * @param userId 用户编号
     * @return 结果
     */
    AjaxResult getInfo(Integer userId);

    Map<String, Object> getUserInfo(TblUser user);

    JsonResult userList(Integer pageNum,Integer pageSize,String userName);

    JsonResult getUserMenu(Integer loginId);

    AjaxResult captcha();

    JsonResult disable(Integer userId,String disableTime);
}
