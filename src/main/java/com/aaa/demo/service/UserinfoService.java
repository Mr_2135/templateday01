package com.aaa.demo.service;

import com.aaa.demo.entity.Userinfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
public interface UserinfoService extends IService<Userinfo> {

}
