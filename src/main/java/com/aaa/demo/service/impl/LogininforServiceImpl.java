package com.aaa.demo.service.impl;

import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;

import com.aaa.demo.common.MyUtils;
import com.aaa.demo.entity.Logininfor;
import com.aaa.demo.entity.resultmodel.JsonResult;
import com.aaa.demo.mapper.LogininforMapper;
import com.aaa.demo.service.LogininforService;
import com.aaa.demo.util.MyConstants;
import com.aaa.demo.util.ServletUtils;
import com.aaa.demo.util.ip.AddressUtils;
import com.aaa.demo.util.ip.IpUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Mr.2135
 * @since 2021-12-27
 */
@Slf4j
@Service
public class LogininforServiceImpl extends ServiceImpl<LogininforMapper, Logininfor> implements LogininforService {
    @Autowired
    private LogininforMapper logininforMapper;
    @Override
    public void recordLogininfor(Logininfor logininfor)
    {
        logininforMapper.insertLogininfor(logininfor);
    }

    @Override
    public JsonResult selectLogininforList(String loginName, String loginLocation, String status, String beginTime, String endTime, Integer pageNum, Integer pageSize) {
        Integer count = logininforMapper.selectLogininforCount(loginName, loginLocation, status, beginTime, endTime);
        Map<String, Object> info = MyUtils.getPageInfo(pageNum, pageSize, count);
        Integer page = Integer.valueOf(info.get("start").toString());
        List<Logininfor> list = logininforMapper.selectLogininforList(loginName, loginLocation, status, beginTime, endTime, page, pageSize);
        info.put("data",list);
        return JsonResult.ok(info);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public JsonResult deleteLogininforByIds(Long[] infoIds) {
        return JsonResult.toAjax(logininforMapper.deleteLogininforByIds(infoIds));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public JsonResult cleanLogininfor() {
        logininforMapper.cleanLogininfor();
        return JsonResult.okMsg(MyConstants.OPERATION_SUCCESS_MESSAGE);
    }

    public static String getBlock(Object msg)
    {
        if (msg == null)
        {
            msg = "";
        }
        return "[" + msg.toString() + "]";
    }
}
