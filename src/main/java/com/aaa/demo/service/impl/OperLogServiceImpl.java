package com.aaa.demo.service.impl;


import com.aaa.demo.common.MyUtils;
import com.aaa.demo.entity.OperLog;
import com.aaa.demo.entity.resultmodel.JsonResult;
import com.aaa.demo.mapper.OperLogMapper;
import com.aaa.demo.service.OperLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 操作日志记录 服务实现类
 * </p>
 *
 * @author Mr.2135
 * @since 2022-02-09
 */
@Service
public class OperLogServiceImpl extends ServiceImpl<OperLogMapper, OperLog> implements OperLogService {

    @Autowired
    private OperLogMapper operLogMapper;

    @Override
    public void insertOperlog(OperLog operLog) {
        operLogMapper.insertOperlog(operLog);
    }

    @Override
    public JsonResult selectOperLogList(String title, Integer businessType, String status,
                                        String operName, String beginTime, String endTime,
                                        Integer pageNum, Integer pageSize)
    {
        Integer count = operLogMapper.selectOperLogCount(title, businessType, status, operName, beginTime, endTime);
        Map<String, Object> map = MyUtils.getPageInfo(pageNum, pageSize, count);
        Integer page = Integer.parseInt(map.get("start").toString());
        List<OperLog> operLogs = operLogMapper.selectOperLogList(title, businessType, status, operName, beginTime, endTime, page, pageSize);
        map.put("data",operLogs);
        return JsonResult.ok(map);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public JsonResult deleteOperLogByIds(Long[] operIds) {
        return JsonResult.toAjax(operLogMapper.deleteOperLogByIds(operIds));
    }

    @Override
    public JsonResult selectOperLogById(Long operId) {
        return JsonResult.ok(operLogMapper.selectOperLogById(operId));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void cleanOperLog() {
        operLogMapper.cleanOperLog();
    }
}
