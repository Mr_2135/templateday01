package com.aaa.demo.service.impl;

import com.aaa.demo.service.TblDeptService;
import com.aaa.demo.common.MyUtils;
import com.aaa.demo.entity.TblDept;
import com.aaa.demo.entity.resultmodel.JsonResult;
import com.aaa.demo.mapper.TblDeptMapper;
import com.aaa.demo.util.sg.AjaxError;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.aaa.demo.util.MyConstants;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 部门表 服务实现类
 * </p>
 *
 * @author cwp

 */
@Service
public class TblDeptServiceImpl extends ServiceImpl<TblDeptMapper, TblDept> implements TblDeptService {

    @Autowired
    private TblDeptMapper deptMapper;
    @Override
    public List<TblDept> deptList( String deptName) {
        List<TblDept> depts = deptMapper.deptList(deptName);
        return depts;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public JsonResult deptAdd(TblDept dept) {
        if (deptMapper.deptConut(dept.getDeptName()) > 0){
            return JsonResult.errorMsg("部门名称重复");
        }
        return JsonResult.toAjax(deptMapper.deptAdd(dept));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public JsonResult deptUpdate(TblDept dept) {
        if (deptMapper.deptConut(dept.getDeptName(),dept.getDeptId()) > 0){
            return JsonResult.errorMsg("部门名称重复");
        }
        return JsonResult.toAjax(deptMapper.deptUpdate(dept));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public JsonResult deptDelete(TblDept dept) {
        return JsonResult.toAjax(deptMapper.deptDelete(dept));
    }
}
