package com.aaa.demo.service.impl;

import com.aaa.demo.entity.TblUser;
import com.aaa.demo.entity.resultmodel.AjaxResult;
import com.aaa.demo.entity.resultmodel.LayUiTree;
import com.aaa.demo.service.TblMenuService;
import com.aaa.demo.entity.TblMenu;
import com.aaa.demo.mapper.TblMenuMapper;
import com.aaa.demo.util.TreeUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 菜单权限表 服务实现类
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
@Service
public class TblMenuServiceImpl extends ServiceImpl<TblMenuMapper, TblMenu> implements TblMenuService {

    @Autowired
    private TblMenuMapper menuMapper;

    @Override
    public Set<String> selectMenuByUserId(TblUser admin) {
        Set<String> permsSet = new HashSet<>();
        if (admin.isAdmin()){
            permsSet.add("*:*:*");
        }else {
            List<String> perms = menuMapper.selectMenuPermsByUserId(admin.getUserId());
            for (String perm : perms) {
                if (StringUtils.isNotBlank(perm)){
                    permsSet.addAll(Arrays.asList(perm.trim().split(",")));
                    permsSet.addAll(perms);
                }
            }
        }
        return permsSet;
    }

    @Override
    public AjaxResult getRouters(Integer userId) {
        List<TblMenu> menus;
        if (new TblUser().setUserId(userId).isAdmin()){
            menus = menuMapper.selectMenuTreeAll();
        }else{
            menus = menuMapper.menuTreeList(userId);
        }
        List<LayUiTree> layUiTrees = TreeUtil.fromMenuListToTreeList(menus);
        return AjaxResult.success(layUiTrees);
    }
}
