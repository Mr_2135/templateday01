package com.aaa.demo.service.impl;

import com.aaa.demo.service.TblRoleMenuService;
import com.aaa.demo.entity.TblRoleMenu;
import com.aaa.demo.mapper.TblRoleMenuMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色和菜单关联表 服务实现类
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
@Service
public class TblRoleMenuServiceImpl extends ServiceImpl<TblRoleMenuMapper, TblRoleMenu> implements TblRoleMenuService {

}
