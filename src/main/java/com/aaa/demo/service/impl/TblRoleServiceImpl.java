package com.aaa.demo.service.impl;

import com.aaa.demo.service.TblRoleService;
import com.aaa.demo.common.MyUtils;
import com.aaa.demo.entity.TblRole;
import com.aaa.demo.entity.resultmodel.JsonResult;
import com.aaa.demo.mapper.TblRoleMapper;
import com.aaa.demo.mapper.TblRoleMenuMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.aaa.demo.util.MyConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 角色信息表 服务实现类
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
@Service
public class TblRoleServiceImpl extends ServiceImpl<TblRoleMapper, TblRole> implements TblRoleService {
    @Autowired
    private TblRoleMapper roleMapper;

    @Autowired
    private TblRoleMenuMapper roleMenuMapper;

    @Override
    public JsonResult roleList(Integer pageNum, Integer pageSize, String roleByName) {
        int count = roleMapper.roleCount(roleByName);
        Map<String, Object> map = MyUtils.getPageInfo(pageNum, pageSize, count);
        int start = Integer.parseInt(map.get("start").toString());
        List<TblRole> users = roleMapper.roleList(start, pageSize, roleByName);
        return JsonResult.ok(users);
    }

    @Override
    public JsonResult roleAdd(TblRole role ,Integer[] menuId) {
        if (role == null && menuId.length == 0)
            return JsonResult.errorMsg(MyConstants.DATA_IS_EMPTY);
        role.setStatus("0");
        int i = roleMapper.roleAdd(role);
        if (i>0){
            roleMenuMapper.addRoleMenu(role.getRoleId(), menuId);
            return JsonResult.okMsg(MyConstants.OPERATION_SUCCESS_MESSAGE);
        }
        return JsonResult.errorMsg(MyConstants.OPERATION_FAIL_MESSAGE);
    }

    @Override
    public JsonResult updateRole(TblRole role) {
        if (role == null)
            return JsonResult.errorMsg(MyConstants.DATA_IS_EMPTY);
        int i = roleMapper.updateRole(role);
        if (i>0){
            roleMenuMapper.deleteRoleMenu(role.getRoleId());
            roleMenuMapper.addRoleMenu(role.getRoleId(),role.getMenuId());
            return JsonResult.okMsg(MyConstants.OPERATION_SUCCESS_MESSAGE);
        }
        return JsonResult.errorMsg(MyConstants.OPERATION_FAIL_MESSAGE);
    }
}
