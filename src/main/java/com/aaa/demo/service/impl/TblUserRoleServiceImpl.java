package com.aaa.demo.service.impl;

import com.aaa.demo.entity.TblUserRole;
import com.aaa.demo.mapper.TblUserRoleMapper;
import com.aaa.demo.service.TblUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户和角色关联表 服务实现类
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
@Service
public class TblUserRoleServiceImpl extends ServiceImpl<TblUserRoleMapper, TblUserRole> implements TblUserRoleService {

}
