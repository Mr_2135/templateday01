package com.aaa.demo.service.impl;

import cn.dev33.satoken.secure.SaSecureUtil;
import cn.dev33.satoken.stp.SaLoginModel;
import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import com.aaa.demo.common.MyUtils;
import com.aaa.demo.constant.Constants;
import com.aaa.demo.entity.TblMenu;
import com.aaa.demo.entity.TblUser;
import com.aaa.demo.entity.resultmodel.AjaxResult;
import com.aaa.demo.entity.resultmodel.LayUiTree;
import com.aaa.demo.enums.UserStatus;
import com.aaa.demo.manager.AsyncManager;
import com.aaa.demo.manager.factory.AsyncFactory;
import com.aaa.demo.mapper.TblUserMapper;
import com.aaa.demo.redis.RedisCache;
import com.aaa.demo.service.TblMenuService;
import com.aaa.demo.service.TblUserService;
import com.aaa.demo.util.MyConstants;
import com.aaa.demo.util.ServletUtils;
import com.aaa.demo.util.TreeUtil;
import com.aaa.demo.entity.resultmodel.JsonResult;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wf.captcha.ArithmeticCaptcha;
import com.wf.captcha.base.Captcha;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
@Service
public class TblUserServiceImpl extends ServiceImpl<TblUserMapper, TblUser> implements TblUserService {

    @Autowired
    private TblUserMapper userMapper;
    @Autowired
    private RedisCache redisCache;


    @Override
    public AjaxResult login(String username, String password, String code, String uuid) {
        TblUser user = userMapper.selectByUserName(username);
        if (user == null){
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, "用户不存在"));
            return AjaxResult.error(UserStatus.DOESNOTEXIST.getInfo());
        }
//        if (StringUtils.isEmpty(code) || !code.equals(redisCache.getCacheObject(MyConstants.CAPTCHA+uuid))){
//            return AjaxResult.error(MyConstants.VERIFICATION_CODE_ERROR);
//        }
        if (UserStatus.DISABLE.getCode().equals(user.getStatus())){
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, "账户已停用"));
            return AjaxResult.error(UserStatus.DISABLE.getInfo());
        }
        if (!user.getPassword().equals(SaSecureUtil.md5BySalt(password,user.getSalt()))){
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, "账号或密码错误"));
            return AjaxResult.error(MyConstants.ACCOUNT_PASSWORD_ERROR);
        }
        UserAgent userAgent = UserAgentUtil.parse(ServletUtils.getRequest().getHeader("User-Agent"));
        AjaxResult result = AjaxResult.success();
        StpUtil.setLoginId(user.getUserId(), userAgent.getOs().getName());
        SaTokenInfo tokenInfo = StpUtil.getTokenInfo();
        result.put("token",tokenInfo.getTokenValue());
        return result;
    }

    @Override
    public AjaxResult getInfo(Integer userId) {

        return null;
    }

    @Override
    public Map<String, Object> getUserInfo(TblUser user) {
        return userMapper.getUserInfo(user.getUserId());
    }

    @Override
    public JsonResult userList(Integer pageNum, Integer pageSize, String userName) {
        int count = userMapper.userListCount(userName);
        Map<String, Object> map = MyUtils.getPageInfo(pageNum, pageSize, count);
        int start = Integer.parseInt(map.get("start").toString());
        List<TblUser> users = userMapper.userList(start, pageSize, userName);
        return JsonResult.ok(users);
    }

    @Override
    public JsonResult getUserMenu(Integer loginId) {
        List<TblMenu> menus = userMapper.selectMenuByUserId(loginId);
        List<LayUiTree> trees = TreeUtil.fromMenuListToTreeList(menus);
        return JsonResult.ok(trees);
    }

    @Override
    public AjaxResult captcha() {
        try {
            ArithmeticCaptcha captcha = new ArithmeticCaptcha(101, 38);
            captcha.setFont(Captcha.FONT_1);
            // 几位数运算，默认是两位
            captcha.setLen(2);
            // 获取运算的结果：5
            String text = captcha.text();
            String base64 = captcha.toBase64();
            //获取唯一id
            String key = UUID.randomUUID().toString();
            redisCache.setCacheObject(MyConstants.CAPTCHA+key,text,MyConstants.CAPTCHA_EXPIRATION, TimeUnit.MINUTES);
            AjaxResult result = AjaxResult.success();
            result.put("img",base64);
            result.put("uuid",key);
            return result;
        } catch (Exception e) {
            return AjaxResult.error("生成失败");
        }

    }

    @Override
    public JsonResult disable(Integer userId, String disableTime) {
        if (userMapper.selectByUserId(userId) != null){
            if (StpUtil.isDisable(userId)){
                return JsonResult.errorMsg(MyConstants.ACCOUNT_IS_DISABLED);
            }

//            StpUtil.disable(userId,)
        }
        return JsonResult.errorMsg(MyConstants.USER_DOES_NOT_EXIST);
    }

}
