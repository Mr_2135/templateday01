package com.aaa.demo.service.impl;

import com.aaa.demo.entity.Userinfo;
import com.aaa.demo.mapper.UserinfoMapper;
import com.aaa.demo.service.UserinfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cwp
 * @since 2021-05-24
 */
@Service
public class UserinfoServiceImpl extends ServiceImpl<UserinfoMapper, Userinfo> implements UserinfoService {

}
