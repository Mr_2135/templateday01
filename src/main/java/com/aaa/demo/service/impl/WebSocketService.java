package com.aaa.demo.service.impl;


import cn.hutool.core.map.MapUtil;
import cn.hutool.json.JSONUtil;
import com.aaa.demo.redis.ApplicationConextUtils;
import com.aaa.demo.redis.RedisCache;
import com.aaa.demo.util.NbUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * @description: websocket 测试
 * @author: Mr.2135
 * @date: 2022-04-15 12:13
 */
@Component
@Slf4j
@Service
@ServerEndpoint("/api/websocket/{deviceId}")
public class WebSocketService {
    /**
     * 当前在线连接数
     */
    private static int onlineCount = 0;
    /**
     * 存放每个客户端对应的MyWebSocket对象
     */
    private static final Map<String, WebSocketService> WEB_SOCKET_SET = new ConcurrentHashMap<>();

    /**
     * 当前会话
     */
    private Session session;

    /**
     * 设备id
     */
    private String deviceId;

    /**
     * 建立连接
     * @param session 会话
     * @param deviceId 设备ID
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("deviceId") String deviceId)
    {
        addOnlineCount();
        log.info("现在来连接的客户ID：{};设备ID：{}",session,deviceId);
        this.deviceId = deviceId;
        this.session = session;
        log.info("当前在线的用户数量：{}",getOnlineCount());
        try{
            //messageType 1代表上线 2代表下线 3代表在线名单 4代表普通消息
            //先给所有人发送通知，说我上线了
            Map<String, Object> online = new HashMap<>(16);
            online.put("messageType","上线");
            online.put("deviceId",deviceId);
            sendMessageAll(JSONUtil.toJsonStr(online),deviceId);
            //把自己的信息加入到map当中去
            WEB_SOCKET_SET.put(deviceId, this);
            log.info("有连接关闭！ 当前在线人数" + getOnlineCount());
            //给自己发一条消息：告诉自己现在都有谁在线
            Map<String,Object> forMyself = MapUtil.newHashMap();
            forMyself.put("messageType","在线名单");
            //移除掉自己
            Set<String> set = WEB_SOCKET_SET.keySet();
            forMyself.put("onlineDevice",set);
            sendMessageTo(JSONUtil.toJsonStr(forMyself),deviceId);
            RedisCache redisCache = ApplicationConextUtils.getBean(RedisCache.class);
            redisCache.setCacheObject("MyDate:"+deviceId,getTimeMillis(),20, TimeUnit.HOURS);

        }catch (Exception e){
            log.info("建立连接时发生问题");
        }
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose() {
        subOnlineCount();
        WEB_SOCKET_SET.remove(deviceId);
        try {
            //messageType 1代表上线 2代表下线 3代表在线名单  4代表普通消息
            Map<String,Object> map1 = MapUtil.newHashMap();
            map1.put("messageType","下线");
            map1.put("onlineDevice",WEB_SOCKET_SET.keySet());
            map1.put("device",deviceId);
            sendMessageAll(JSONUtil.toJsonStr(map1),deviceId);
            RedisCache redisCache = ApplicationConextUtils.getBean(RedisCache.class);
            String date = redisCache.getCacheObject("MyDate:" + deviceId).toString();
            redisCache.deleteObject("MyDate:" + deviceId);
            log.info("在线时间：{}",getTimeMillis() - Long.parseLong(date));
        }
        catch (IOException e){
            log.info(deviceId+"下线的时候通知所有人发生了错误");
        }
        log.info("有连接关闭！ 当前在线人数" + getOnlineCount());

    }

    public void sendMessageTo(String message, String toDeviceId) {
        for (WebSocketService item : WEB_SOCKET_SET.values()) {
            log.info("在线设备名单：{}",item.deviceId);
            if (item.deviceId.equals(toDeviceId) ) {
                item.session.getAsyncRemote().sendText(message);
                break;
            }
        }
    }

    public void sendMessageAll(String message,String fromDeviceId) throws IOException {
        for (WebSocketService item : WEB_SOCKET_SET.values()) {
            item.session.getAsyncRemote().sendText(message);
        }
    }

    /**
     * @ Param session
     * @ Param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }

    /**
     * 实现服务器主动推送
     */
    public void sendMessage(String message) throws IOException {
        this.session.getBasicRemote().sendText(message);
    }

    public static synchronized int getOnlineCount() {
        return onlineCount;
    }

    public static synchronized void addOnlineCount() {
        WebSocketService.onlineCount++;
    }

    public static synchronized void subOnlineCount() {
        WebSocketService.onlineCount--;
    }

    /**
     * 将当前时间转成时间戳/秒
     * @return 时间戳
     */
    public static Long getTimeMillis()
    {
        return (System.currentTimeMillis() / 1000);
    }

}
