package com.aaa.demo.util;


import cn.hutool.crypto.symmetric.AES;
import org.apache.commons.codec.binary.Base64;

/**
 * 解密
 * AES-128-CBC 可以自己定义“密钥”和“偏移量“。
 * AES-128 是jdk自动生成的“密钥”。
 */
public class AesCbcUtil {

    /**
     * AES解密
     *
     * @param data           //密文，被加密的数据
     * @param key            //秘钥
     * @param iv             //偏移量
     * @return
     * @throws Exception
     */
    public static String decrypt(String data, String key, String iv){
        //加密秘钥
        byte[] keyByte = Base64.decodeBase64(key);
        //偏移量
        byte[] ivByte = Base64.decodeBase64(iv);
        // AES aes = new AES(Mode.CTS, Padding.PKCS5Padding, keyByte, ivByte);
        // iOS 安卓端
        AES aes = new AES("CBC",
                "PKCS7Padding",
                // 密钥，可以自定义
                keyByte,
                // iv加盐，按照实际需求添加
                ivByte);
        // 解密
        return aes.decryptStr(data);
    }


}
