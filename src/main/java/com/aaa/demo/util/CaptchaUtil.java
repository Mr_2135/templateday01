package com.aaa.demo.util;


import com.aaa.demo.util.sg.AjaxError;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @description:
 * @author: simon2135
 * @date: 2021-07-15 15:12
 */
public class CaptchaUtil {



    private static int x = 17;
    private static int fontSize = 28;//字体大小
    private static  int y = 28;
    private static int codeCount = 4;// 定义图片上显示验证码的个数
    private static char[] codeSequence = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
                        'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
    public static Map<String,String> getCaptcha(int width, int height) throws IOException {
        Map<String,String> map = new HashMap<>();
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics graphics = image.getGraphics();
        Random random = new Random();// 创建一个随机数生成器类
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0,0,width,height);
        Font font = new Font("楷体", Font.BOLD, fontSize);
        graphics.setFont(font);
        graphics.setColor(Color.BLUE);
        graphics.drawRect(0, 0, width - 1, height - 1);
        // randomCode用于保存随机产生的验证码，以便用户登录后进行验证。
        StringBuffer randomCode = new StringBuffer();
        int red = 0, green = 0, blue = 0;
        // 随机产生codeCount数字的验证码。
        for (int i = 0; i < codeCount; i++) {
            // 得到随机产生的验证码数字。
            String code = String.valueOf(codeSequence[random.nextInt(codeSequence.length)]);
            // 产生随机的颜色分量来构造颜色值，这样输出的每位数字的颜色值都将不同。
            red = random.nextInt(255);
            green = random.nextInt(255);
            blue = random.nextInt(255);
            // 用随机产生的颜色将验证码绘制到图像中。
            graphics.setColor(new Color(red, green, blue));
            graphics.drawString(code, (i + 1) * x, y);
            // 将产生的四个随机数组合在一起。
            randomCode.append(code);
        }
        // 随机产生40条干扰线，使图象中的认证码不易被其它程序探测到。
        graphics.setColor(Color.BLACK);
        for (int i = 0; i < 30; i++) {
            int x = random.nextInt(width);
            int y = random.nextInt(height);
            int xl = random.nextInt(12);
            int yl = random.nextInt(12);
            graphics.drawLine(x, y, x + xl, y + yl);
        }
//        BufferedImage转成 base64字符串
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        ImageIO.write(image,"png",stream);
        String base64 = Base64.getEncoder().encodeToString(stream.toByteArray());
        map.put("code",randomCode.toString());
        map.put("image","data:image/png;base64,"+base64);
        return map;
    }

    public static void main(String[] args) throws IOException {
        Map<String, String> captcha = CaptchaUtil.getCaptcha(108, 38);
        System.out.println(captcha);
    }
}
