package com.aaa.demo.util;

import cn.hutool.core.util.RuntimeUtil;

/**
 * @description: 命令行工具类
 * @author: Mr.2135
 * @date: 2021-09-18 10:13
 * 用于执行命令行命令（在Windows下是cmd，在Linux下是shell命令）
 */
public class CommandLine {
    public static String getCommand(String str){
        String line = RuntimeUtil.execForStr(str);
        return line;
    }

    public static void main(String[] args) {
    }
}
