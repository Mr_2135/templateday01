package com.aaa.demo.util;

import cn.hutool.poi.excel.ExcelReader;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.entity.ContentType;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * @description: exce工具类
 * @author: Mr.2135
 * @date: 2021-07-02 10:50
 */
@Slf4j
public class ExcelUtil {
    public static List<Map<String,String>> getExce(MultipartFile file){
        try {
            HSSFWorkbook sheets = new HSSFWorkbook(file.getInputStream());
            List<Map<String,String>> list = new ArrayList<>();
            //循环工作表sheet
            for (int numSheet = 0; numSheet < sheets.getNumberOfSheets(); numSheet++) {
                HSSFSheet sheetAt = sheets.getSheetAt(numSheet);
                if (sheetAt == null){
                    continue;
                }
                // 循环行row
                for (int rowNum = 1; rowNum < sheetAt.getLastRowNum(); rowNum++) {
                    HSSFRow row = sheetAt.getRow(rowNum);
                    Map<String,String> map = new HashMap<>();
                    if (row == null){
                        continue;
                    }
                    if (row.getCell(0) !=null){
                        map.put("studentId",getValue(row.getCell(0)));
                    }
                    if (row.getCell(1) !=null){
                        map.put("name",getValue(row.getCell(1)));
                    }
                    if (row.getCell(2) !=null){
                        map.put("snowCloud",getValue(row.getCell(2)));
                    }
                    if (row.getCell(3) !=null){
                        map.put("className",getValue(row.getCell(3)));
                    }
                    if (row.getCell(4) !=null){
                        map.put("sex",getValue(row.getCell(4)));
                    }
                    list.add(map);
                }
            }
            return list;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getValue(HSSFCell hssfCell) {
        if (hssfCell.getCellType() == CellType.BOOLEAN) {
            // 返回布尔类型的值
            return String.valueOf(hssfCell.getBooleanCellValue());
        } else if (hssfCell.getCellType() ==  CellType.NUMERIC) {
            // 返回数值类型的值
            return String.valueOf(hssfCell.getNumericCellValue());
        } else {
            // 返回字符串类型的值
            return String.valueOf(hssfCell.getStringCellValue());
        }
    }
    public void  generatePdf()
    {

    }
    public static void main(String[] args){
        String filePath = "E:/Download/learn/test.xls";
        ExcelReader reader = cn.hutool.poi.excel.ExcelUtil.getReader(new File(filePath));
        List<Map<String, Object>> maps = reader.readAll();
        if (maps.size()>0){
            maps.forEach(map->{
                Set<String> sets = map.keySet();
                for (String set : sets) {
                    log.info("key:{},values:{}",set,map.get(set));
                }
            });
        }else {
            System.out.println("获取失败");
        }
    }
}
