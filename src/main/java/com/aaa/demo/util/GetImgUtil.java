package com.aaa.demo.util;

import lombok.extern.slf4j.Slf4j;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @description:
 * @author: Administrator
 * @date: 2020-12-02 9:51
 */
@Slf4j
public class GetImgUtil {

    // 获取要取得的帧数
    private static final int fifthFrame = 25;

    /**
     * @param filePath 需要截取帧的视频的字节输入流
     * @return
     */
    public static String getImg(String filePath) {
        log.info("视频地址："+filePath);
        FFmpegFrameGrabber grabber;
        try {
            grabber = FFmpegFrameGrabber.createDefault(filePath);
            grabber.start();
            // 视频总帧数
            int videoLength = grabber.getLengthInFrames();
            Frame frame = null;
            int i = 0;
            while (i < videoLength) {
                // 过滤前5帧,因为前5帧可能是全黑的
                frame = grabber.grabFrame();
                if ((i > fifthFrame) && (frame.image != null)) {
                    break;
                }
                i++;
            }
            //视频旋转度
            Java2DFrameConverter converter = new Java2DFrameConverter();
            // 绘制图片
            BufferedImage bi = converter.getBufferedImage(frame);
            //图片的类型
            String imageMat = "jpg";
            String targetFileName= new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
            String imgName = targetFileName+ "."+imageMat;
            //图片的完整路径
            String imagePath = "" + imgName;
            log.info("图片保存路径"+imagePath);
            //创建文件
            File output = new File(imagePath);
            if(!output.exists()){
                output.mkdirs();
            }
            ImageIO.write(bi, imageMat, output);
            grabber.stop();
            grabber.close();
            return imgName;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
