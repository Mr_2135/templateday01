package com.aaa.demo.util;

import cn.hutool.core.img.ImgUtil;
import lombok.extern.slf4j.Slf4j;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import org.springframework.core.io.ClassPathResource;

import javax.servlet.ServletOutputStream;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.Date;

/**
 * @description: 图片工具类
 * @author: Mr.simon2135
 * @date: 2021-08-20 11:17
 */
@Slf4j
public class ImageUtil {
    private static Integer TWO = 2;
    private static Integer THREE = 3;
    private static Integer FOUR = 4;

    /**
     * 根据模板图片，添加文字
     * @param username
     * @param out
     */
public static void addImgText(String username, ServletOutputStream out){
        try {
            ClassPathResource resource = new ClassPathResource("/static/img.jpg");
            BufferedImage read = ImgUtil.read(resource.getInputStream());
            String date = NbUtil.getDateString(new Date(), "yyyy-MM-dd");
            String[] split = date.split("-");
            Font font = new Font("楷体", Font.BOLD, 80);
            Graphics graphics = read.getGraphics();
            graphics.setColor(new Color(42,73,128));
            graphics.setFont(font);
//            graphics.drawString(username,563,1155);
            int length = username.length();
            // 根据姓名的长度决定生成的位置
            if (length == TWO){
                graphics.drawString(username,610,1155);
            }else if (length == THREE){
                graphics.drawString(username,563,1155);
            }else if (length == FOUR){
                graphics.drawString(username,495,1155);
            }else {
                graphics.drawString(username,563,1155);
            }
            graphics.drawString(split[0],2330,2063);
            graphics.drawString(split[1],2578,2063);
            graphics.drawString(split[2],2741,2063);
            graphics.dispose();
            ImgUtil.write(read,"png",out);
            out.flush();
            out.close();
        } catch (IOException e) {
            log.info("结业证生成错误：{}",e.getMessage());
        }
    }

    /**
     * 根据微信头像地址将头像保存到本地
     * @param avatarPath
     * @throws IOException
     */
    public static String downloadAvatar(String avatarPath,String localPath){
        try {
            BufferedImage read = ImgUtil.read(new URL(avatarPath));
            File dest = new File("E:/upload/"+NbUtil.getDateString(new Date(),"yyyyMMddHHmmss")+".png");
            FileOutputStream out = new FileOutputStream(dest);
            ImgUtil.write(read,"png",out);
            out.close();
            return dest.getName();
        } catch (IOException e) {
            log.info("微信头像保存失败：{}",e.getMessage());
        }
        return null;
    }
    public static BufferedImage removeBackgroundImage(BufferedImage imgage) throws IOException {
        ImageIcon imageIcon = new ImageIcon(imgage);
        BufferedImage bufferedImage = new BufferedImage(imageIcon.getIconWidth(), imageIcon.getIconHeight(),
                BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D g2D = (Graphics2D) bufferedImage.getGraphics();
        g2D.drawImage(imageIcon.getImage(), 0, 0, imageIcon.getImageObserver());
        int alpha = 0;
        for (int j1 = bufferedImage.getMinY(); j1 < bufferedImage.getHeight(); j1++) {
            for (int j2 = bufferedImage.getMinX(); j2 < bufferedImage.getWidth(); j2++) {
                int rgb = bufferedImage.getRGB(j2, j1);
                int R = (rgb & 0xff0000) >> 16;
                int G = (rgb & 0xff00) >> 8;
                int B = (rgb & 0xff);
                if (((255 - R) < 30) && ((255 - G) < 30) && ((255 - B) < 30)) {
                    rgb = ((alpha + 1) << 24) | (rgb & 0x00ffffff);
                }
                bufferedImage.setRGB(j2, j1, rgb);
            }
        }
        g2D.drawImage(bufferedImage, 0, 0, imageIcon.getImageObserver());
        return bufferedImage;
    }

    /**
     * 图片识别文字
     * @param imageFileUrl
     */
    public static String recognizeText(String imageFileUrl){
        try {
            Tesseract tesseract = new Tesseract();
            tesseract.setDatapath("tessdata");
            tesseract.setLanguage("chi_sim");
            String result = tesseract.doOCR(new File(imageFileUrl));
            log.info("图片路径：{}",imageFileUrl);
            log.info("读取到文字：{}",result);
            return result;
        } catch (TesseractException e) {
            log.info("文字识别异常：{}",e.getMessage());
        }
        return null;
    }
    // outputStream转inputStream
    public static ByteArrayInputStream parse(final OutputStream out) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        baos = (ByteArrayOutputStream) out;
        final ByteArrayInputStream swapStream = new ByteArrayInputStream(baos.toByteArray());
        return swapStream;
    }


    public static void main(String[] args) {
        //https://blog.csdn.net/qq_44132240/article/details/113330403
//        OutputStream out =null;
//        String content = "https://www.hutool.cn/docs/#/";
//        new SimpleQrcodeGenerator().generate(content).toStream(out);
//        BufferedImage read = ImgUtil.read("E:/upload/demo.png");
        BufferedImage img = ImgUtil.read("E:/upload/11111.jpg");
        BufferedImage image = new BufferedImage(600, 700, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = (Graphics2D) image.getGraphics();
        // 消除锯齿感
        graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        graphics.drawImage(img,0,0,null);
        Font font = new Font("楷体", Font.BOLD, 20);
        graphics.setFont(font);
        graphics.setColor(Color.WHITE);
        graphics.drawString("学校：郑州大学",20,285);
        graphics.dispose();
        ImgUtil.write(image,new File("E:/upload/2222.jpg"));

    }
}
