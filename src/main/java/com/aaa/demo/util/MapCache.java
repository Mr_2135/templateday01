package com.aaa.demo.util;

import lombok.extern.slf4j.Slf4j;
import net.jodah.expiringmap.ExpirationPolicy;
import net.jodah.expiringmap.ExpiringMap;

import java.util.concurrent.TimeUnit;

/**
 * @description: 全局Map
 * @author: Administrator
 * @date: 2021-05-18 11:06
 */

@Slf4j
public class MapCache {
    private static ExpiringMap<Object, Object> cacheMap = ExpiringMap.builder().expiration(2, TimeUnit.MINUTES)
            .expirationPolicy(ExpirationPolicy.CREATED)
            .build();

    public static void destoryCacheMap() {
        cacheMap = null;
    }

    public static ExpiringMap<Object, Object> getCacheMap() {
        return cacheMap;
    }

    public static void set(Object key, Object values) {
        cacheMap.put(key, values);
    }

    public static Object get(Object key) {
        return cacheMap.get(key);
    }

    public static String getString(Object key) {
        return (String) cacheMap.get(key);
    }

    public static boolean containsKey(Object key){
        return cacheMap.containsKey(key);
    }


    public static Object getToEmpty(Object key) {
        Object o = cacheMap.get(key);
        if (o == null)
            return "";
        else
            return o;
    }

    public static void remove(Object key) {
        cacheMap.remove(key);
    }

    public static void clear() {
        cacheMap.clear();
    }

    public static void main(String[] args) {

    }
}
