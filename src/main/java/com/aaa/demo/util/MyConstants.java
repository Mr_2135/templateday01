package com.aaa.demo.util;


public class MyConstants {
    //保存操作
    public static final String SAVE_OPERATION = "save";
    //修改操作
    public static final String UPDATE_OPERATION = "update";
    //修改操作
    public static final String DELETE_OPERATION = "delete";
    //加密算法
    public static final String ALGORITHM_NAME = "MD5";
    /**
     * 加密的key
     */
    public static final String SECRET_KEY = "wyh123456";
    //加密次数
    public static final int HASH_ITERATIONS = 1000;
    /**
     * 操作成功信息
     */
    public static final String OPERATION_SUCCESS_MESSAGE = "操作成功";
    /**
     * 操作成功代码
     */
    public static final int OPERATION_SUCCESS_CODE = 200;
    /**
     * 操作失败信息
     */
    public static final String OPERATION_FAIL_MESSAGE = "操作失败";

    public static final String ACCOUNT_IS_DISABLED = "账号被禁用";

    public static final String DATA_IS_EMPTY = "数据为空";

    public static final String VERIFICATION_CODE_ERROR = "验证码错误";

    public static final String  ACCOUNT_PASSWORD_ERROR= "账号密码错误";

    public static final String  USER_DOES_NOT_EXIST= "用户不存在";
    /**
     * 操作失败代码
     */
    public static final int OPERATION_FAIL_CODE = 1;
    /**
     * 验证码有效期（分钟）
     */
    public static final Integer CAPTCHA_EXPIRATION = 5;
    /**
     * 验证码
     */
    public static final String  CAPTCHA = "captcha:";
    //
    public static final String EMPTY = "";

    public static final String UNDERLINE = "_";

    public static final String INTEGRAL = "积分";


}
