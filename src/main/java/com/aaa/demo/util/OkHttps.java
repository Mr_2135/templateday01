package com.aaa.demo.util;

import com.ejlchina.okhttps.HTTP;
import okhttp3.*;
import okio.ByteString;

import java.util.concurrent.TimeUnit;
import java.util.stream.DoubleStream;

/**
 * @description:
 * @author: Administrator
 * @date: 2022-06-11 18:06
 */
public class OkHttps {

    public static void main(String[] args) {
        String url = "ws://localhost:8081/api/websocket/00-15-5D-8A-14-1A";
        Request request = new Request.Builder().get().url(url).build();
        // WebSocket
        HTTP http = HTTP.builder()
                .config((OkHttpClient.Builder builder) -> {

                    // 配置 WebSocket 心跳间隔（默认没有心跳）
                    builder.pingInterval(10, TimeUnit.SECONDS);
                })
                .build();
        WebSocket websocket = http.webSocket(request, new WebSocketListener() {
            @Override
            public void onOpen(WebSocket webSocket, Response response) {
                super.onOpen(webSocket, response);
                webSocket.send("1111");
                //连接成功...
            }

            @Override
            public void onMessage(WebSocket webSocket, String text) {
                super.onMessage(webSocket, text);
                //收到消息...（一般是这里处理json）
            }

            @Override
            public void onMessage(WebSocket webSocket, ByteString bytes) {
                super.onMessage(webSocket, bytes);
                //收到消息...（一般很少这种消息）
            }

            @Override
            public void onClosed(WebSocket webSocket, int code, String reason) {
                super.onClosed(webSocket, code, reason);
                //连接关闭...
            }

            @Override
            public void onFailure(WebSocket webSocket, Throwable throwable, Response response) {
                super.onFailure(webSocket, throwable, response);
                //连接失败...
            }
        });
    }

}
