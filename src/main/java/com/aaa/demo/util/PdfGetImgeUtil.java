package com.aaa.demo.util;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * @description:
 * @author: Mr.2135
 * @date: 2021-11-10 15:32
 */
public class PdfGetImgeUtil {
    /**
     * 转换全部的pdf
     * @param fileAddress 文件地址
     * @param filename PDF文件名
     * @param type 图片类型
     */
    public static void pdf2png(String fileAddress,String filename,String type) {
        // 将pdf装图片 并且自定义图片得格式大小
        File file = new File(fileAddress+"\\"+filename+".pdf");
        try {
            PDDocument doc = PDDocument.load(file);
            PDFRenderer renderer = new PDFRenderer(doc);
            int pageCount = doc.getNumberOfPages();
            BufferedImage bufferedImage = null;
            String path = null;
            for (int i = 0; i < pageCount; i++) {
                BufferedImage image = renderer.renderImageWithDPI(i, 144); // Windows native DPI
                // BufferedImag srcImage = resize(image, 240, 240);//产生缩略图
                ImageIO.write(image, type, new File("E:\\upload\\pdf"+"\\"+filename+"_"+i+"."+type));
//                bufferedImage = bufferedImage(bufferedImage,image);
//                path = "E:\\upload\\pdf"+filename+"_"+i+"."+type;
//                if (pageCount == (i+1)){
//                    ImageIO.write(bufferedImage, type, new File(path));
//                }
            }
            doc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     *自由确定起始页和终止页
     * @param fileAddress 文件地址
     * @param filename pdf文件名
     * @param indexOfStart 开始页  开始转换的页码，从0开始
     * @param indexOfEnd 结束页  停止转换的页码，-1为全部
     * @param type 图片类型
     */
    public static void pdf2png(String fileAddress,String filename,int indexOfStart,int indexOfEnd,String type) {
        // 将pdf装图片 并且自定义图片得格式大小
        File file = new File(fileAddress+"\\"+filename+".pdf");
        try {
            PDDocument doc = PDDocument.load(file);
            PDFRenderer renderer = new PDFRenderer(doc);
            for (int i = indexOfStart; i < indexOfEnd; i++) {
                // Windows native DPI
                BufferedImage image = renderer.renderImageWithDPI(i, 174);
                // 产生缩略图
                // BufferedImage srcImage = resize(image, 240, 240);
                ImageIO.write(image, type, new File(fileAddress+"\\"+filename+"_"+(i+1)+"."+type));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static BufferedImage bufferedImage (BufferedImage a,BufferedImage b){
        if (a == null){
            return b;
        }
        int width = a.getWidth();
        int height = a.getHeight() + b.getHeight();
        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = bufferedImage.createGraphics();
        graphics.drawImage(a,0,0,a.getWidth(),a.getHeight(),null);
        graphics.drawImage(b,0,a.getHeight(),b.getWidth(),b.getHeight(),null);
        graphics.dispose();
        return bufferedImage;
    }

    public static void main(String[] args) throws IOException {
//        String fileAddress = "E:\\exam\\test";
//        String filename = "1408355847817531392";
//        pdf2png(fileAddress,filename,"png");
//        BufferedImage bufferedImage = bufferedImage(ImgUtil.read("E:\\upload\\11111.jpg"), ImgUtil.read("E:\\upload\\aun2r-y0v0b.jpg"));
//        ImageIO.write(bufferedImage, "png", new File(fileAddress+"\\"+"aaaa"+"."+"png"));
        String paht = "E:\\upload\\pdf\\2021年度鹤壁市教育信息化培训(2)_1.png";

    }
}
