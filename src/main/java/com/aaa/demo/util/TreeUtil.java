package com.aaa.demo.util;



import com.aaa.demo.entity.resultmodel.LayUiTree;
import com.aaa.demo.entity.TblMenu;

import java.util.ArrayList;
import java.util.List;

//动态左侧
public class TreeUtil {

    public static List<LayUiTree> fromMenuListToTreeList(List<TblMenu> menuList) {
        List<LayUiTree> treeList = new ArrayList<>();
        //遍历所有的menu对象，然后发现menu对象有孩子，就继续便利孩子，递归操作

        for (TblMenu menu : menuList) {
            //如果父亲的id为0，就说明是一级目录
            if (menu.getParentId() == 0) {
                //将menu转成tree对象
                LayUiTree tree = fromMenuToTree(menu);
                //找自己的孩子,给tree对象设置孩子children
                LayUiTree treeChilren = setTreeChilren(tree, menuList);
                treeList.add(treeChilren);
            }
        }
        return treeList;
    }


    public static LayUiTree setTreeChilren(LayUiTree tree, List<TblMenu> menuList) {
        //此集合封装所有的孩子
        List<LayUiTree> children = new ArrayList<>();
        for (TblMenu menu : menuList) {
            //tree的id是他所有孩子的父亲id
            if (menu.getParentId() == tree.getId()) {
                //将menu转成tree对象
                LayUiTree layUiTree = fromMenuToTree(menu);
                children.add(layUiTree);
            }
        }
        tree.setChildren(children);
        return tree;
    }

    public static LayUiTree fromMenuToTree(TblMenu menu) {
        LayUiTree layUiTree = new LayUiTree();
        layUiTree.setId(menu.getMenuId());
        layUiTree.setTitle(menu.getMenuName());
        layUiTree.setUrl(menu.getUrl());
        layUiTree.setIcon(menu.getIcon());
        return layUiTree;
    }
}
