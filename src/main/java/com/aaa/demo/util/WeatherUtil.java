package com.aaa.demo.util;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.aaa.demo.entity.weather.WeatherData;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * @description: 天气
 * @author: Mr.2135
 * @date: 2022-06-22 10:04
 */
@Slf4j
public class WeatherUtil {
    public static void main(String[] args) {
        // 可以自己用
        Map<String, Object> map = new HashMap<>(16);
        map.put("stationid",57083);
        map.put("_",NbUtil.getTimeMillis());
        String str = HttpUtil.get("http://www.nmc.cn/rest/weather", map);
        JSONObject obj = JSONUtil.parseObj(str);
        WeatherData data = obj.get("data", WeatherData.class);
        log.info("data参数：{}",data.getPredict().getDetail());
    }
}
