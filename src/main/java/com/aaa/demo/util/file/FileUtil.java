package com.aaa.demo.util.file;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Objects;

/**
 * @description:
 * @author: Administrator
 * @date: 2021-08-21 11:43
 */
public class FileUtil {

    private static String wxAvatar="E:\\upload\\avatar";

    /**
     *   微信头像保存到本地地址
     */
    public static void getFileUrl(String fileUrl) throws IOException {
        URL url1 = new URL(fileUrl);
        URLConnection connection = url1.openConnection();
        InputStream input = connection.getInputStream();
        OutputStream out= null;

    }
    /**
     * 获取文件名的后缀
     *
     * @param file 表单文件
     * @return 后缀名
     */
    public static String getExtension(MultipartFile file)
    {
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        if (StringUtils.isBlank(extension))
        {
            extension = MimeTypeUtils.getExtension(Objects.requireNonNull(file.getContentType()));
        }
        return extension;
    }
}
