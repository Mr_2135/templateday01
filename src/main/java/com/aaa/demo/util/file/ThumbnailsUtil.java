package com.aaa.demo.util.file;

import cn.hutool.http.HttpUtil;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hpsf.Thumbnail;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;

/**
 * @description: 图片压缩工具
 * @author: Mr.215
 * @date: 2022-03-16 17:59
 */
public class ThumbnailsUtil {

    /**
     * 图片默认宽度
     */
    private Integer width = 160;
    /**
     * 图片默认高度
     */
    private Integer height = 160;


    /**
     * 创建缩略图
     * @return 图片地址
     */
//    public String createThumbnail(Integer width,Integer height,String imgPath) throws IOException {
//        width = StringUtils.isBlank(width.toString()) ? this.width :width;
//        height = StringUtils.isBlank(height.toString()) ? this.height :height;
//        URL url1 = new URL(imgPath);
//        InputStream inputStream = url1.openConnection().getInputStream();
//        BufferedImage image = ImageIO.read(inputStream);
//        BufferedImage bufferedImage = Thumbnails.of(image)
//                .size(width, height)
//                .asBufferedImage();
//        return OssUtils.uploadFile("",getMultipartFile(bufferedImage));
//    }

    public MultipartFile getMultipartFile(BufferedImage image)
    {
        try {
            //创建一个ByteArrayOutputStream
            //把BufferedImage写入ByteArrayOutputStream
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(image, "png", os);
            //ByteArrayOutputStream转成InputStream
            InputStream input = new ByteArrayInputStream(os.toByteArray());
            //InputStream转成MultipartFile
            return new MockMultipartFile("file", "file.jpg", "text/plain", input);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  null;
    }

}
