package com.aaa.demo;

import cn.dev33.satoken.secure.SaSecureUtil;
import cn.hutool.bloomfilter.BitMapBloomFilter;
import cn.hutool.core.codec.Base64;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.io.FileTypeUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.core.io.resource.ResourceUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.crypto.digest.MD5;
import cn.hutool.crypto.symmetric.AES;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import cn.smallbun.screw.core.Configuration;
import cn.smallbun.screw.core.engine.EngineConfig;
import cn.smallbun.screw.core.engine.EngineFileType;
import cn.smallbun.screw.core.engine.EngineTemplateType;
import cn.smallbun.screw.core.execute.DocumentationExecute;
import cn.smallbun.screw.core.process.ProcessConfig;
import com.aaa.demo.common.MyUtils;
import com.aaa.demo.entity.TblDept;
import com.aaa.demo.entity.TblMenu;
import com.aaa.demo.entity.TblUser;
import com.aaa.demo.entity.resultmodel.LayUiTree;
import com.aaa.demo.enums.GradeEnum;
import com.aaa.demo.mapper.TblDeptMapper;
import com.aaa.demo.redis.RedisCache;
import com.aaa.demo.util.CaptchaUtil;
import com.aaa.demo.util.MyConstants;
import com.arcsoft.face.*;
import com.arcsoft.face.enums.DetectMode;
import com.arcsoft.face.enums.DetectOrient;
import com.arcsoft.face.enums.ErrorInfo;
import com.arcsoft.face.toolkit.ImageInfo;
import com.freewayso.image.combiner.ImageCombiner;
import com.freewayso.image.combiner.element.TextElement;
import com.freewayso.image.combiner.enums.Direction;
import com.freewayso.image.combiner.enums.OutputFormat;
import com.freewayso.image.combiner.enums.ZoomMode;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Test;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.quartz.CronExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static com.arcsoft.face.toolkit.ImageFactory.getRGBData;
import static org.opencv.highgui.HighGui.imshow;
import static org.opencv.imgproc.Imgproc.COLOR_RGB2GRAY;

@SpringBootTest
@Slf4j
class DemoApplicationTests {

    @Autowired
    private RedisCache redisCache;
    @Autowired
    private TblDeptMapper deptMapper;

    public static final String SCORE_RANK = "score_rank";

    private static final double SIMILARITY =0.8;
    @Test
    void contextLoads() {
//        Set<ZSetOperations.TypedTuple<String>> tuples = new HashSet<>();
//        long start = System.currentTimeMillis();
//        for (int i = 0; i < 100; i++) {
//            DefaultTypedTuple<String> tuple = new DefaultTypedTuple<>("张三" + i, 1D + i);
//            tuples.add(tuple);
//        }
//        System.out.println("循环时间:" +( System.currentTimeMillis() - start));
//        Long num = redisCache.redisTemplate.opsForZSet().add(SCORE_RANK, tuples);
//        System.out.println("批量新增时间:" +(System.currentTimeMillis() - start));
//        System.out.println("受影响行数：" + num);

        redisCache.redisTemplate.opsForZSet().remove(SCORE_RANK,"张三99");
        Set<String> range = redisCache.redisTemplate.opsForZSet().reverseRangeWithScores(SCORE_RANK, 0, 9);
        System.out.println(JSONUtil.toJsonStr(range));
    }
    @Test
    void jsoupTest() throws IOException {
        String url = "https://iflow-api.xuexi.cn/logflow/api/v1/client_event";
        Document document = Jsoup.connect(url).get();
        log.info("获取页面内容：{}",document.getAllElements());

    }
    @SneakyThrows
    @Test
    void poiTest()  {
        ExcelReader reader = ExcelUtil.getReader(FileUtil.file("E:\\Download\\learn\\test.xls"));
        List<Map<String, Object>> maps = reader.readAll();
        AtomicInteger i= new AtomicInteger(1);
        maps.forEach(map -> {
            System.out.println("第"+i+"行："+map);
            i.getAndIncrement();
        });
    }

    /**
     * 将图片白色背景去掉
     * @throws IOException
     */
    @Test
    void imgTest() throws IOException {
        String url = "E:/upload/微信图片_20210819181832.jpg";
        BufferedImage read = ImgUtil.read(url);
        ImageIcon imageIcon = new ImageIcon(read);
        BufferedImage bufferedImage = new BufferedImage(imageIcon.getIconWidth(), imageIcon.getIconHeight(),
                BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D g2D = (Graphics2D) bufferedImage.getGraphics();
        g2D.drawImage(imageIcon.getImage(), 0, 0, imageIcon.getImageObserver());
        int alpha = 0;
        for (int j1 = bufferedImage.getMinY(); j1 < bufferedImage.getHeight(); j1++) {
            for (int j2 = bufferedImage.getMinX(); j2 < bufferedImage.getWidth(); j2++) {
                int rgb = bufferedImage.getRGB(j2, j1);
                int R = (rgb & 0xff0000) >> 16;
                int G = (rgb & 0xff00) >> 8;
                int B = (rgb & 0xff);
                if (((255 - R) < 30) && ((255 - G) < 30) && ((255 - B) < 30)) {
                    rgb = ((alpha + 1) << 24) | (rgb & 0x00ffffff);
                }
                bufferedImage.setRGB(j2, j1, rgb);
            }
        }
        g2D.drawImage(bufferedImage, 0, 0, imageIcon.getImageObserver());
        File dest = new File("E:/upload/aun2r-y0v0b.jpg");
        FileOutputStream outputStream = new FileOutputStream(dest);
        ImgUtil.write(bufferedImage,"png",outputStream);
    }
    @Test
    void date(){
    }
    @Test
    void getArcSoft(){
        //从官网获取
        String APP_ID = "5B4v3HHd8c9RQEGQD9VGX9aR68HqsWZ5QNFataP81e6S";
        String SDK_KEY = "BsWDbfPXULJEqNhCFXJTuHXG8vEj68SVKbusBKVMFLrG";
        FaceEngine faceEngine = new FaceEngine("E:\\SDK\\libs\\WIN64");
        //激活引擎
        int errorCode = faceEngine.activeOnline(APP_ID, SDK_KEY);

        if (errorCode != ErrorInfo.MOK.getValue() && errorCode != ErrorInfo.MERR_ASF_ALREADY_ACTIVATED.getValue()) {
            System.out.println("引擎激活失败");
        }
        ActiveFileInfo activeFileInfo=new ActiveFileInfo();
        errorCode = faceEngine.getActiveFileInfo(activeFileInfo);
        if (errorCode != ErrorInfo.MOK.getValue() && errorCode != ErrorInfo.MERR_ASF_ALREADY_ACTIVATED.getValue()) {
            System.out.println("获取激活文件信息失败");
        }
        //引擎配置
        EngineConfiguration engineConfiguration = new EngineConfiguration();
        engineConfiguration.setDetectMode(DetectMode.ASF_DETECT_MODE_IMAGE);
        engineConfiguration.setDetectFaceOrientPriority(DetectOrient.ASF_OP_ALL_OUT);
        engineConfiguration.setDetectFaceMaxNum(10);
        engineConfiguration.setDetectFaceScaleVal(16);
        //功能配置
        FunctionConfiguration functionConfiguration = new FunctionConfiguration();
        functionConfiguration.setSupportAge(true);
        functionConfiguration.setSupportFace3dAngle(true);
        functionConfiguration.setSupportFaceDetect(true);
        functionConfiguration.setSupportFaceRecognition(true);
        functionConfiguration.setSupportGender(true);
        functionConfiguration.setSupportLiveness(true);
        functionConfiguration.setSupportIRLiveness(true);
        engineConfiguration.setFunctionConfiguration(functionConfiguration);
        //初始化引擎
        errorCode = faceEngine.init(engineConfiguration);
        if (errorCode != ErrorInfo.MOK.getValue()) {
            System.out.println("初始化引擎失败");
        }
        //人脸检测 new File("E:/upload/1.png")
        ImageInfo imageInfo = getRGBData(FileUtil.file("E:/upload/2.png"));
        List<FaceInfo> faceInfoList = new ArrayList<FaceInfo>();
        errorCode = faceEngine.detectFaces(imageInfo.getImageData(), imageInfo.getWidth(), imageInfo.getHeight(), imageInfo.getImageFormat(), faceInfoList);
        System.out.println(faceInfoList);
        //特征提取
        FaceFeature faceFeature = new FaceFeature();
        errorCode = faceEngine.extractFaceFeature(imageInfo.getImageData(), imageInfo.getWidth(), imageInfo.getHeight(), imageInfo.getImageFormat(), faceInfoList.get(0), faceFeature);
        System.out.println("特征值大小：" + faceFeature.getFeatureData().length);
        //特征提取2
       List<String> list = new ArrayList<>();
       list.add("E:/upload/a33va-4pfm2.jpg");
       list.add("E:/upload/1.png");
       list.add("E:/upload/23.jpg");
        for (int i = 0; i < list.size(); i++) {
            //人脸检测2
            ImageInfo imageInfo2 = getRGBData(FileUtil.file(list.get(i)));
            List<FaceInfo> faceInfoList2 = new ArrayList<FaceInfo>();
            errorCode = faceEngine.detectFaces(imageInfo2.getImageData(), imageInfo2.getWidth(), imageInfo2.getHeight(),imageInfo.getImageFormat(), faceInfoList2);
            System.out.println(faceInfoList);

            //特征提取2
            FaceFeature faceFeature2 = new FaceFeature();
            errorCode = faceEngine.extractFaceFeature(imageInfo2.getImageData(), imageInfo2.getWidth(), imageInfo2.getHeight(), imageInfo.getImageFormat(), faceInfoList2.get(0), faceFeature2);
            System.out.println("特征值大小：" + faceFeature.getFeatureData().length);

            //特征比对
            FaceFeature targetFaceFeature = new FaceFeature();
            targetFaceFeature.setFeatureData(faceFeature.getFeatureData());
            FaceFeature sourceFaceFeature = new FaceFeature();
            sourceFaceFeature.setFeatureData(faceFeature2.getFeatureData());
            FaceSimilar faceSimilar = new FaceSimilar();
            errorCode = faceEngine.compareFaceFeature(targetFaceFeature, sourceFaceFeature, faceSimilar);
            if (SIMILARITY == faceSimilar.getScore() || SIMILARITY < faceSimilar.getScore()){
                System.out.println("==================");
                log.info("匹配成功的文件路径：{}",list.get(i));
                log.info("匹配成功,相似度：{}",faceSimilar.getScore());
            }else {
                log.info("匹配不成功的相似度：{}",faceSimilar.getScore());
            }
        }
    }

    /**
     * 通过反射追加
     */
    @Test
    void reflect(){
        TblUser user = new TblUser();
        user.setCreateTime(LocalDateTime.now());
        ReflectUtil.invoke(user,"setLoginName","admin");
        System.out.println(user.getLoginName());
    }
    @Test
    void fileTest(){
        File[] ls = FileUtil.ls("E:\\app");
        for (File l : ls) {
            String type = FileTypeUtil.getType(FileUtil.file(l.getPath()));
            System.out.println("文件路径"+l.getPath());
            System.out.println("文件名称"+l.getName()+"文件类型："+type);
        }

    }
    @Test
    void bloomFilter(){
        // 初始化
        BitMapBloomFilter filter = new BitMapBloomFilter(10);
        for (int i = 0; i <= 10000; i++) {
             filter.add(""+i);
        }
        // 查找
        log.info("是否存在：{}",filter.contains("8889"));
    }
    @Test
    void redisTest(){
        TblMenu menu01 = new TblMenu()
                .setMenuId(1)
                .setMenuName("系统管理")
                .setParentId(0)
                .setUrl("");
        TblMenu menu02 = new TblMenu()
                .setMenuId(3)
                .setMenuName("用户管理")
                .setParentId(1)
                .setUrl("httttsss");
        TblMenu menu03 = new TblMenu()
                .setMenuId(2)
                .setMenuName("店铺管理")
                .setParentId(0)
                .setUrl("");
        TblMenu menu04 = new TblMenu()
                .setMenuId(4)
                .setMenuName("商品管理")
                .setParentId(2)
                .setUrl("httttssse");
        List<TblMenu> nodeList = CollUtil.newArrayList();
            nodeList.add(menu01);
            nodeList.add(menu02);
            nodeList.add(menu03);
            nodeList.add(menu04);
        List<LayUiTree> lists = new ArrayList<>();
        for (TblMenu tblMenu : nodeList) {
            if (tblMenu.getParentId() == 0){
                LayUiTree tree = new LayUiTree();
                tree.setId(tblMenu.getMenuId());
                tree.setTitle(tblMenu.getMenuName());
                tree.setField(tblMenu.getParentId()+"");
                tree.setUrl(tblMenu.getUrl());
                for (TblMenu menu : nodeList){
                    List<LayUiTree> list = new ArrayList<>();
                    if (menu.getParentId() == tree.getId()){
                        LayUiTree trees = new LayUiTree();
                        trees.setId(menu.getMenuId());
                        trees.setTitle(menu.getMenuName());
                        trees.setField(menu.getParentId()+"");
                        trees.setUrl(menu.getUrl());
                        list.add(trees);
                    }
                    tree.setChildren(list);
                }
                lists.add(tree);
            }

        }
        for (LayUiTree list : lists) {
            log.info("tree:{}",list);
        }
//        List<Tree<String>> build = TreeUtil.build(nodeList, "0",(menu, tree) -> {
//            tree.setId(String.valueOf(menu.getMenuId()));
//            tree.setName(menu.getMenuName());
//            tree.setParentId(String.valueOf(menu.getParentId()));
//            tree.putExtra("url",menu.getUrl());
//            tree.putExtra("other","aah");
//        });
//        for (Tree<String> stringTree : build) {
//            System.out.println(
//                    stringTree
//            );
//        }
    }
    @Test
    void opevTest(){
//        ClassPathResource resource = new ClassPathResource("opcv/opencv_java454.dll");
//        System.load(resource.getAbsolutePath());
//        System.out.println("Welcome to OpenCV " + Core.VERSION);
//        BufferedImage read = ImgUtil.read("E:\\upload\\a33va-4pfm2.jpg");
//        if (read == null){
//            log.info("图片获取失败");
//        }
//        Mat grayImage = new Mat(read.getHeight(), read.getWidth(), CvType.CV_8SC1);
//        // 进行图像色彩空间转换
////        cvtColor(read, grayImage, COLOR_RGB2GRAY);
//        imshow("Processed Image", grayImage);
        // 随机生成密钥

//        Map<String,Object> map = new HashMap<>();
//        map.put("password",654321);
//        map.put("username","wyh");
//        map.put("time","2021-12-7");
//        String key = DigestUtil.md5Hex(MyConstants.SECRET_KEY);
//        System.out.println(key);
//        AES aes = SecureUtil.aes(key.getBytes());
//        log.info("str:{}",map.toString());
//        String encryptHex = aes.encryptHex(map.toString());
//        log.info("加密后的数据：{}",encryptHex);
//        String content = "691c953c278a9609afcfdc070bb41c0a7b589beca4c144d730dfcf8f1dbbb24592058a3b6544c97bde831882241a9205";
//        String key = DigestUtil.md5Hex(MyConstants.SECRET_KEY);
//        AES aes = SecureUtil.aes(key.getBytes());
//        String decryptStr = aes.decryptStr(content, CharsetUtil.CHARSET_UTF_8);
//        System.out.println(decryptStr);
//        System.out.println(GradeEnum.valueOf());
//        Date date = DateUtils.addDays(new Date(), 30);
//        System.out.println(DateFormatUtils.format(date,"yyyy-MM-dd HH:mm:ss"));
//        String str = SaSecureUtil.md5BySalt("123456", "9ed6610c-6132-4d0c-a8a4-5011080ba754");
//        System.out.println(str);
//        List<Integer> list = new ArrayList<>();
//        list.add(101);
//        list.add(102);
//        list.add(104);
//        Set<String> sets = deptMapper.selectDeptByList(list);
//        System.out.println(String.join(",",sets));
        //数据源
//        try {
//            Map<String, String> captcha = CaptchaUtil.getCaptcha(108, 38);
//            System.out.println(captcha);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        String salt = "621c8b2ad7e1914bc88579ff";
//        String password = SecureUtil.md5("nxk410600" + salt);
//        System.out.println(password);
//        boolean b = StringUtils.startsWith("onlineDevice:176", "onlineDevice:");
//        System.out.println(b);
//        long parse = DateUtil.parse("2022-07-22 18:10:13").getTime() / 1000;
//        long time = DateUtil.date().getTime() / 1000;
//        System.out.println(parse - time);
        // getAccessToken: "at.cxoh2blib0ygj89j7x7p2f9g0m6fm033-99sgbgxeq1-1ojzl3n-zxrdxnf42"
        Map<String, Object> map = MapUtil.newHashMap();
        map.put("accessToken","at.cxoh2blib0ygj89j7x7p2f9g0m6fm033-99sgbgxeq1-1ojzl3n-zxrdxnf42");
        map.put("deviceSerial","J08415190");
        map.put("channel",1);
        String s = HttpUtil.post("https://open.ys7.com/api/lapp/device/info",map);
        log.info("参数：{}",JSONUtil.parseObj(s));
    }

    @Test
    public void tstClass() throws ClassNotFoundException {
//        Class<?> myTestJob = Class.forName("com.aaa.demo.quartz.MyTestJob");
//        myTestJob.getName();

    }

    @Test
    public void simpleDemo() throws Exception{
        //合成器（指定背景图和输出格式，整个图片的宽高和相关计算依赖于背景图，所以背景图的大小是个基准）
        ImageCombiner combiner = new ImageCombiner("https://sjb-oss.oss-cn-hangzhou.aliyuncs.com/atd/material/7/image/7f0afcc14e60695f0f28f2bb9801fcf5", OutputFormat.JPG);

        //加图片元素
        combiner.addImageElement("https://sjb-oss.oss-cn-hangzhou.aliyuncs.com/atd/material/7/image/7f0afcc14e60695f0f28f2bb9801fcf5", 0, 300);

        //加文本元素
        combiner.addTextElement("周末大放送", 60, 100, 960);

        //执行图片合并
        combiner.combine();

        //可以获取流（并上传oss等）
        InputStream is = combiner.getCombinedImageStream();

        //也可以保存到本地
        combiner.save("d://image.jpg");
    }

    @Test
    public void imageCombiner() throws Exception {
        // 背景图
        BufferedImage bgImageUrl = ImgUtil.read("E:\\app\\壁纸\\wallhaven-x8ye3z.jpg");

        String qrCodeUrl = "http://xxx.com/image/qrCode.png";               //二维码
        String productImageUrl = "http://xxx.com/image/product.jpg";        //商品图
        //BufferedImage waterMark = ImageIO.read(new URL("https://xxx.com/image/waterMark.jpg")); //水印图
        //BufferedImage avatar = ImageIO.read(new URL("https://xxx.com/image/avatar.jpg"));       //头像
        String title = "# 最爱的家居";                                       //标题文本
        String content = "苏格拉底说：“如果没有那个桌子，可能就没有那个水壶”";  //内容文本

        ImageCombiner combiner = new ImageCombiner(bgImageUrl,1500, 0, ZoomMode.Height, OutputFormat.PNG);  //v1.1.4之后可以指定背
        // 针对背景和整图的设置
        //设置背景高斯模糊（毛玻璃效果）
        combiner.setBackgroundBlur(30);
        //设置整图圆角（输出格式必须为PNG）
        combiner.setCanvasRoundCorner(100);

        //标题（默认字体为阿里普惠、黑色，也可以自己指定Font对象）
        combiner.addTextElement(title, 0, 150, 1400)
                .setCenter(true)        //居中绘制（会忽略x坐标，改为自动计算）
                .setAlpha(.8f)          //透明度（0.0~1.0）
                .setRotate(45)          //旋转（0~360）
                .setColor(Color.red)    //颜色
                .setDirection(Direction.RightLeft); //绘制方向（从右到左，用于需要右对齐场景）

        //内容（设置文本自动换行，需要指定最大宽度（超出则换行）、最大行数（超出则丢弃）、行高）
        combiner.addTextElement(content, "微软雅黑", 40, 150, 1480)
                .setSpace(.5f)                      //字间距
                .setStrikeThrough(true)             //删除线
                .setAutoBreakLine(837, 2, 60);      //自动换行（还有一个LineAlign参数可以指定对齐方式）

        //商品图（设置坐标、宽高和缩放模式，若按宽度缩放，则高度按比例自动计算）
        combiner.addImageElement(productImageUrl, 0, 160, 837, 0, ZoomMode.Width)
                .setCenter(true)        //居中绘制（会忽略x坐标，改为自动计算）
                .setRoundCorner(46);    //设置圆角

        //头像（圆角设置一定的大小，可以把头像变成圆的）
        combiner.addImageElement("", 200, 1200)
                .setRoundCorner(200);   //圆角

        //水印（设置透明度，0.0~1.0）
        combiner.addImageElement("", 630, 1200)
                .setAlpha(.8f)          //透明度（0.0~1.0）
                .setRotate(45)          //旋转（0~360）
                .setBlur(20);           //高斯模糊(1~100)
                //.setRepeat(true, 100, 50);    //平铺绘制（可设置水平、垂直间距）

        //加入圆角矩形元素（版本>=1.2.0），作为二维码的底衬
        combiner.addRectangleElement(138, 1707, 300, 300)
                .setColor(Color.WHITE)
                .setRoundCorner(50)     //该值大于等于宽高时，就是圆形，如设为300
                .setAlpha(.8f);

        //二维码（强制按指定宽度、高度缩放）
        combiner.addImageElement(qrCodeUrl, 138, 1707, 186, 186, ZoomMode.WidthHeight);

        //价格（元素对象也可以直接new，然后手动加入待绘制列表）
        TextElement textPrice = new TextElement("￥1290", 60, 230, 1300);
        textPrice.setColor(Color.red);          //红色
        textPrice.setStrikeThrough(true);       //删除线
        combiner.addElement(textPrice);         //加入待绘制集合

        //执行图片合并
        combiner.combine();

        //可以获取流（并上传oss等）
        InputStream is = combiner.getCombinedImageStream();

        //也可以保存到本地
        //combiner.save("d://image.jpg");
    }

    /**
     * 获取bean名称
     *
     * @param invokeTarget 目标字符串
     * @return bean名称
     */
    public static String getBeanName(String invokeTarget)
    {
        String beanName = StringUtils.substringBefore(invokeTarget, "(");
        return StringUtils.substringBeforeLast(beanName, ".");
    }

    /**
     * 获取bean方法
     *
     * @param invokeTarget 目标字符串
     * @return method方法
     */
    public static String getMethodName(String invokeTarget)
    {
        String methodName = StringUtils.substringBefore(invokeTarget, "(");
        return StringUtils.substringAfterLast(methodName, ".");
    }

    /**
     * 获取method方法参数相关列表
     *
     * @param invokeTarget 目标字符串
     * @return method方法相关参数列表
     */
    public static List<Object[]> getMethodParams(String invokeTarget)
    {
        String methodStr = StringUtils.substringBetween(invokeTarget, "(", ")");
        if (StringUtils.isEmpty(methodStr))
        {
            return null;
        }
        String[] methodParams = methodStr.split(",(?=([^\"']*[\"'][^\"']*[\"'])*[^\"']*$)");
        List<Object[]> classs = new LinkedList<>();
        for (int i = 0; i < methodParams.length; i++)
        {
            String str = StringUtils.trimToEmpty(methodParams[i]);
            // String字符串类型，以'或"开头
            if (StringUtils.startsWithAny(str, "'", "\""))
            {
                classs.add(new Object[] { StringUtils.substring(str, 1, str.length() - 1), String.class });
            }
            // boolean布尔类型，等于true或者false
            else if ("true".equalsIgnoreCase(str) || "false".equalsIgnoreCase(str))
            {
                classs.add(new Object[] { Boolean.valueOf(str), Boolean.class });
            }
            // long长整形，以L结尾
            else if (StringUtils.endsWith(str, "L"))
            {
                classs.add(new Object[] { Long.valueOf(StringUtils.substring(str, 0, str.length() - 1)), Long.class });
            }
            // double浮点类型，以D结尾
            else if (StringUtils.endsWith(str, "D"))
            {
                classs.add(new Object[] { Double.valueOf(StringUtils.substring(str, 0, str.length() - 1)), Double.class });
            }
            // 其他类型归类为整形
            else
            {
                classs.add(new Object[] { Integer.valueOf(str), Integer.class });
            }
        }
        return classs;
    }
}
